package fhn.thaobt5.personalfinancetracker.data.repository


import fhn.thaobt5.personalfinancetracker.data.network.firebase.SavingGoalDataSource
import fhn.thaobt5.personalfinancetracker.data.network.firebase.TransactionDataSource
import fhn.thaobt5.personalfinancetracker.data.network.firebase.UserDataSource
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.models.User
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.Assertions.*

class UserRepositoryFirebaseImplTest {
    @MockK
    lateinit var userDataSource: UserDataSource

    @MockK
    lateinit var transactionDataSource: TransactionDataSource

    @MockK
    lateinit var savingGoalDataSource: SavingGoalDataSource

    @MockK
    lateinit var userRepositoryFirebaseImpl: UserRepositoryFirebaseImpl

    @MockK
    lateinit var user: User

    @MockK
    lateinit var transaction: Transaction

    @Before
    fun setUp(){
        userDataSource = mockk()
        transactionDataSource = mockk()
        savingGoalDataSource = mockk()
        user = mockk()
        transaction = mockk()
        userRepositoryFirebaseImpl = UserRepositoryFirebaseImpl(userDataSource, transactionDataSource, savingGoalDataSource)

    }

    @Test
     fun login() = runTest {
        val state = FireBaseState.Success("")
        coEvery { userDataSource.login("email@gmail.com","12345678") } returns state
        val result = userRepositoryFirebaseImpl.login("email@gmail.com","12345678")
        assertEquals(state, result)
    }

    @Test
    fun signUp() = runTest {
        val state = FireBaseState.Success("")
        coEvery { userDataSource.signUp(user) } returns state
        val result = userRepositoryFirebaseImpl.signUp(user)
        assertEquals(state, result)
    }

    @Test
    fun logout() = runTest {
        val state = FireBaseState.Success("")
        coEvery { userDataSource.logout() } returns state
        val result = userRepositoryFirebaseImpl.logout()
        assertEquals(state, result)

    }



    @Test
    fun addATransaction() = runTest{
        val state = FireBaseState.Success("")
        coEvery { transactionDataSource.addTransaction(transaction) } returns state
        val result = userRepositoryFirebaseImpl.addATransaction(transaction)
        assertEquals(state, result)

    }


    @Test
    fun updateTransaction() = runTest{
        val state = FireBaseState.Success("")
        coEvery { transactionDataSource.updateTransaction(transaction) } returns state
        val result = userRepositoryFirebaseImpl.updateTransaction(transaction)
        assertEquals(state, result)
    }

    @Test
    fun deleteTransaction() = runTest {
        val state = FireBaseState.Success("")
        coEvery { transactionDataSource.deleteTransaction(transaction) } returns state
        val result = userRepositoryFirebaseImpl.deleteTransaction(transaction)
        assertEquals(state, result)

    }

    @org.junit.jupiter.api.Test
    fun getAllTransactionsByCurrentUser() {
    }

    @org.junit.jupiter.api.Test
    fun getSavingGoalByCurrentUserFireBase() {
    }

    @org.junit.jupiter.api.Test
    fun addSavingGoalFireBase() {
    }

    @org.junit.jupiter.api.Test
    fun getCurrentUser() {
    }

    @Test
    fun updateUserBalance() {
    }

    @Test
    fun updateUserExpense() {
    }
}