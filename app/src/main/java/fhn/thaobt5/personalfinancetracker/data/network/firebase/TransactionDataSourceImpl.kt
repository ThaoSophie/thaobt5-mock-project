package fhn.thaobt5.personalfinancetracker.data.network.firebase

import android.util.Log
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import fhn.thaobt5.personalfinancetracker.data.models.FireBaseTransactionModel
import fhn.thaobt5.personalfinancetracker.data.utils.*
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class TransactionDataSourceImpl @Inject constructor(
    private val auth: FirebaseAuth,
    private val referenceDatabase: DatabaseReference
): TransactionDataSource {

    override suspend fun addTransaction(transaction: Transaction): FireBaseState<String> {
        return try {
            val transaction = transaction.copy(
                userId = auth.currentUser!!.uid
            )


            val transactionReference = referenceDatabase.child(TRANSACTION_FIREBASE_PATH)
            transactionReference.child(auth.currentUser!!.uid).child(transaction.tranId.toString())
                .setValue(transaction).await()

            FireBaseState.Success("")

        } catch (exception: FirebaseException) {
            return FireBaseState.Fail(exception.message!!)
        }
    }

    override suspend fun updateTransaction(transaction: Transaction): FireBaseState<String> {
        return try {
            val tranReference = referenceDatabase.child(TRANSACTION_FIREBASE_PATH)
                .child(auth.currentUser!!.uid).child(transaction.tranId.toString())

            tranReference.child(AMOUNT_PATH).setValue(transaction.amount)
            tranReference.child(DATE_PATH).setValue(transaction.date)
            tranReference.child(NOTE_PATH).setValue(transaction.note)
            tranReference.child(TAG_PATH).setValue(transaction.tag)
            tranReference.child(TITLE_PATH).setValue(transaction.title)
            tranReference.child(TRANSACTION_TYPE_PATH).setValue(transaction.transactionType)
            return FireBaseState.Success("")
        } catch (ex: FirebaseException) {
            return FireBaseState.Fail(ex.message!!)
        }

    }

    override suspend fun deleteTransaction(transaction: Transaction): FireBaseState<String> {
        return try {
            val tranReference = referenceDatabase.child(TRANSACTION_FIREBASE_PATH)
                .child(auth.currentUser!!.uid).child(transaction.tranId.toString())
            tranReference.removeValue()
            return FireBaseState.Success("")
        } catch (ex: java.lang.Exception) {
            return FireBaseState.Fail(ex.message!!)

        }

    }

    override suspend fun getUserTransactions(): Flow<List<FireBaseTransactionModel>> {
        val transRef = referenceDatabase.child(TRANSACTION_FIREBASE_PATH).child(auth.currentUser!!.uid)
        val tranUserList = arrayListOf<FireBaseTransactionModel>()
        return callbackFlow {
            val valueEventListener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    tranUserList.clear()
                    for (tran in snapshot.children) {
                        tran.getValue(FireBaseTransactionModel::class.java)
                            ?.let { tranUserList.add(it) }
                    }


                    trySend(tranUserList)
                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

            }
            transRef.addValueEventListener(valueEventListener)
            awaitClose {transRef.removeEventListener(valueEventListener)}
        }

    }
}