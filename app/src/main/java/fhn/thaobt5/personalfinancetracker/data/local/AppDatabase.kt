package fhn.thaobt5.personalfinancetracker.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.models.User

@Database(
    entities = [Transaction::class, User::class],
    version = 2,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getTransactionDao(): TransactionDao
    abstract fun getUserDao(): UserDao
}