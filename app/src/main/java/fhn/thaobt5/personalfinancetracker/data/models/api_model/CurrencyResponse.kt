package fhn.thaobt5.personalfinancetracker.data.models.api_model

data class CurrencyResponse(
    val base: String,
    val lastUpdate: Int,
    val rates: Rates
)