package fhn.thaobt5.personalfinancetracker.data.mapper

import fhn.thaobt5.personalfinancetracker.data.models.FireBaseSavingGoalModel
import fhn.thaobt5.personalfinancetracker.domain.models.SavingGoal

object SavingGoalMapper {
    fun mapFromFirebaseModel(source: List<FireBaseSavingGoalModel>): List<SavingGoal> {
        return source.map {
            SavingGoal(
                title = it.title,
                amount = it.amount,
                startDate = it.startDate,
                endDate = it.endDate
            )
        }
    }

    fun mapFromModelToFirebaseModel(source: SavingGoal): FireBaseSavingGoalModel {
        return FireBaseSavingGoalModel().apply {
            this.title = source.title
            this.amount = source.amount
            this.startDate = source.startDate
            this.endDate = source.endDate
        }

    }
}