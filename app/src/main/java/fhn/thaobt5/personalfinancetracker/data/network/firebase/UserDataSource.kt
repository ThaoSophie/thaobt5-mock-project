package fhn.thaobt5.personalfinancetracker.data.network.firebase

import fhn.thaobt5.personalfinancetracker.domain.models.SavingGoal
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.models.User
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import kotlinx.coroutines.flow.Flow

interface UserDataSource {

    // Login for user
    suspend fun login(email: String, password: String): FireBaseState<String>

    suspend fun signUp(user: User): FireBaseState<String>

    fun logout(): FireBaseState<String>



    suspend fun registerUserBalanceInformation(user: User): FireBaseState<String>

    suspend fun updateUserBalance(balance: Long)
    suspend fun updateUserExpense(expense: Long)

    suspend fun updateUserIncome(income: Long)

    suspend fun updateSavingGoal(savingGoal: SavingGoal)

    // getUser
    fun getCurrentUser(): Flow<User>




}