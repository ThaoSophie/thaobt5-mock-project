package fhn.thaobt5.personalfinancetracker.data.models

import android.os.Parcelable
import com.google.firebase.database.IgnoreExtraProperties
import kotlinx.parcelize.Parcelize


@IgnoreExtraProperties
data class FireBaseSavingGoalModel(
    var userId: String? = null,

    var title: String? = null,

    var amount: Double? = null,

    var startDate: String? = null,

    var endDate: String? = null
)