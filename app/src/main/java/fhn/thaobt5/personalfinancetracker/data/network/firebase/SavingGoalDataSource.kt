package fhn.thaobt5.personalfinancetracker.data.network.firebase

import fhn.thaobt5.personalfinancetracker.data.models.FireBaseSavingGoalModel
import fhn.thaobt5.personalfinancetracker.data.models.FireBaseTransactionModel
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import kotlinx.coroutines.flow.Flow

interface SavingGoalDataSource {

    suspend fun addSavingGoal(fireBaseSavingGoalModel: FireBaseSavingGoalModel): Boolean

    fun getAllSavingGoals(): Flow<List<FireBaseSavingGoalModel>>

//    suspend fun updateTransaction(transaction: Transaction): FireBaseState<String>
//
//    suspend fun deleteTransaction(transaction: Transaction): FireBaseState<String>


}