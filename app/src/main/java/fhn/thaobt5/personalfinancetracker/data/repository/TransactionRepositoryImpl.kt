package fhn.thaobt5.personalfinancetracker.data.repository

import android.annotation.SuppressLint
import android.util.Log
import fhn.thaobt5.personalfinancetracker.data.local.AppDatabase
import fhn.thaobt5.personalfinancetracker.data.local.TransactionDao
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.repository.TransactionRepository
import fhn.thaobt5.personalfinancetracker.domain.utils.TransactionType
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class TransactionRepositoryImpl @Inject constructor(
    private val database: AppDatabase
) : TransactionRepository {

    // insert transaction
    override suspend fun insert(transaction: Transaction) {
        return database.getTransactionDao().insertTransaction(
            transaction
        )
    }

    // update transaction
    override suspend fun update(transaction: Transaction) = database.getTransactionDao()
        .updateTransaction(transaction)

    // delete transaction
    @SuppressLint("LongLogTag")
    override suspend fun delete(transaction: Transaction) {
        database.getTransactionDao()
            .deleteTransactionByID(transaction.tranId)
        Log.d("Thao TransactionRepositoryImpl", "delete: , line=31 ")
    }

    // get all saved transaction list
    override fun getAllTransactions(): Flow<List<Transaction>> = database.getTransactionDao().getAllTransactions()

    // get transactions by type(EXPENSE, INCOME, OVERALL)
    override fun getTransactionByType(
        transactionType: TransactionType,
        userId: String
    ): Flow<List<Transaction>> {
        return if (transactionType == TransactionType.OVERALL) getTransactionsByUserID(userId)
        else database.getTransactionDao().getTransactionByType(transactionType, userId)
    }

    // get transactions by userId
    override fun getTransactionsByUserID(userId: String): Flow<List<Transaction>> =
        database.getTransactionDao().getTransactionsByUserID(userId)

    // get transactions by transaction id
    override fun getTransactionByTranID(tranId: Int): Flow<Transaction> {
        return database.getTransactionDao().getTransactionByTranID(tranId)
    }

    // delete transaction by Id
    override suspend fun deleteTransactionByID(tranId: Int) =
        database.getTransactionDao().deleteTransactionByID(tranId)
}