package fhn.thaobt5.personalfinancetracker.data.models

import com.google.firebase.database.IgnoreExtraProperties
import fhn.thaobt5.personalfinancetracker.domain.utils.TransactionTag
import fhn.thaobt5.personalfinancetracker.domain.utils.TransactionType
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

@IgnoreExtraProperties
class FireBaseTransactionModel(
    var userId: String? = null,

    var title: String? = null,

    var amount: Double? = null,

    var transactionType: TransactionType? = null,

    var tag: TransactionTag? = null,

    var date: String? = null,

    var note: String? = null,

    var tranId: Int? = null,

    var createdAt: Long? = null,

    val createdAtDateFormat: String? = null
) {



//    val createdAtDateFormat: String
//        get() = DateFormat.getDateTimeInstance()
//            .format(createdAt)  // Date Format: Jan 11, 2021, 11:30 AM
}
