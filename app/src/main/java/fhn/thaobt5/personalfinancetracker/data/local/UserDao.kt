package fhn.thaobt5.personalfinancetracker.data.local


import androidx.room.*
import fhn.thaobt5.personalfinancetracker.data.utils.USER_TABLE
import fhn.thaobt5.personalfinancetracker.domain.models.User
import kotlinx.coroutines.flow.Flow


@Dao
interface UserDao {

    @Query("SELECT * FROM $USER_TABLE")
    fun getUsers(): Flow<List<User>>

    @Query("SELECT * FROM $USER_TABLE WHERE email = :email")
    fun getUserByEmail(email: String): Flow<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(user: User)

    @Query("DELETE FROM $USER_TABLE WHERE email = :email")
    fun deleteUserByEmail(email: String)







}