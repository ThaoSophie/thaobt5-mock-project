package fhn.thaobt5.personalfinancetracker.data.network.api

import fhn.thaobt5.personalfinancetracker.data.models.api_model.CurrencyResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

private const val API_KEY = "i13fv6d1kg8gchks185avgrvtun69sksgogmjucfbd7gmtisamb6718"
private const val FULL_URL = "https://anyapi.io/api/v1/exchange/rates?base=EUR&apiKey=i13fv6d1kg8gchks185avgrvtun69sksgogmjucfbd7gmtisamb6718"

interface CurrencyApi {


    @GET("api/v1/exchange/rates?apiKey=i13fv6d1kg8gchks185avgrvtun69sksgogmjucfbd7gmtisamb6718")
    suspend fun getRates(
        @Query("base") base: String
    ): Response<CurrencyResponse>

}