package fhn.thaobt5.personalfinancetracker.data.local

import android.content.SharedPreferences
import androidx.core.content.edit
import fhn.thaobt5.personalfinancetracker.utils.*
import javax.inject.Inject

class SharedPreferenceHelper @Inject constructor(
    private val sharedPreferences: SharedPreferences
) {

    // put email
    fun setEmail(email: String){
       sharedPreferences.edit {
           putString(EMAIL, email)
           apply()
       }
    }


    // get email
    fun getEmail() = sharedPreferences.getString(EMAIL, "")


    // set password
    fun setPassword(password: String) {
        sharedPreferences.edit{
            putString(PASSWORD, password)
            apply()
        }

    }

    //get password
    fun getPassword() = sharedPreferences.getString(PASSWORD, "")

    //set user_name
    fun setUserName(userName: String){
        sharedPreferences.edit {
            putString(USER_NAME, userName)
            apply()
        }
    }

    //get user_name
    fun getUSerName() = sharedPreferences.getString(USER_NAME, "")


    // set boolean: isRemember
    fun setIsRemember(isRemember: Boolean){
        sharedPreferences.edit {
            putBoolean(REMEMBER, isRemember)
            apply()
        }
    }

    //get boolean
    fun getIsRemember() = sharedPreferences.getBoolean(REMEMBER, false)

    //set boolean: isLoggedIn
    fun setIsLoggedIn(isLoggedIn: Boolean) {
        sharedPreferences.edit {
            putBoolean(IS_LOGGED_IN, isLoggedIn)
            apply()
        }
    }

    //get boolean
    fun getIsLoggedIn() = sharedPreferences.getBoolean(IS_LOGGED_IN, false)



    fun setIsOpenFirstTime(isOpenFirstTime: Boolean) {
        sharedPreferences.edit {
            putBoolean(IS_OPEN_FIRST_TIME, isOpenFirstTime)
            apply()
        }
    }

    fun getIsOpenFirstTime() = sharedPreferences.getBoolean(IS_OPEN_FIRST_TIME, true)

}