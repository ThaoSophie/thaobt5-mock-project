package fhn.thaobt5.personalfinancetracker.data.network.firebase


import android.util.Log
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import fhn.thaobt5.personalfinancetracker.data.utils.TRANSACTION_FIREBASE_PATH
import fhn.thaobt5.personalfinancetracker.data.utils.USER_FIREBASE_PATH
import fhn.thaobt5.personalfinancetracker.domain.models.SavingGoal
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.models.User
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await
import javax.inject.Inject


class UserDataSourceImpl @Inject constructor(
    private val firebaseAuth: FirebaseAuth,
    private val referenceDatabase: DatabaseReference
): UserDataSource  {

    override suspend fun signUp(user: User): FireBaseState<String> {
        return try {
            firebaseAuth.createUserWithEmailAndPassword(
                user.email,
                user.password
            ).await()

            if (firebaseAuth.currentUser != null) {
                val userFirebaseRegister = User(
                    userId = firebaseAuth.currentUser!!.uid,
                    userName = user.userName,
                    email = user.email,
                    password = user.password,
                    balance = user.balance,
                    totalExpense = user.totalExpense,
                    totalIncome = user.totalIncome
                )

                val userReference = referenceDatabase.child(USER_FIREBASE_PATH)
                userReference.child(firebaseAuth.currentUser!!.uid)
                    .setValue(userFirebaseRegister).await()
                FireBaseState.Success("")
            } else {
                FireBaseState.Fail("")

            }
        }

        catch (exception: FirebaseException) {
            return FireBaseState.Fail(exception.message!!)
        }
    }

    override fun logout(): FireBaseState<String> {
        return try {
            firebaseAuth.signOut()
            FireBaseState.Success("")
        } catch (exception: FirebaseException) {
            Log.d(TAG, "logout: can't logout")
            return FireBaseState.Fail(exception.message!!)
        }
    }

    override suspend fun registerUserBalanceInformation(user: User): FireBaseState<String> {
        return try {
            val userFirebaseRegister = User(
                userId = firebaseAuth.currentUser!!.uid,
                userName = user.userName,
                email = user.email,
                password = user.password,
                balance = user.balance,
                totalExpense = user.totalExpense,
                totalIncome = user.totalIncome
            )

            val userReference = referenceDatabase.child(USER_FIREBASE_PATH)
            userReference.child(userFirebaseRegister.userId)
                .setValue(userFirebaseRegister).await()

            firebaseAuth.signOut()

            FireBaseState.Success("")

        } catch (ex: FirebaseException) {
            return FireBaseState.Fail(ex.message!!)
        }
    }

    override suspend fun updateUserBalance(balance: Long) {
        if (firebaseAuth.currentUser != null) {
            referenceDatabase.child(USER_FIREBASE_PATH).child(firebaseAuth.currentUser!!.uid)
                .child("balance")
                .setValue(balance)
        }
    }

    override suspend fun updateUserExpense(expense: Long) {
        if (firebaseAuth.currentUser != null) {
            referenceDatabase.child(USER_FIREBASE_PATH).child(firebaseAuth.currentUser!!.uid)
                .child("totalExpense")
                .setValue(expense)
        }

    }

    override suspend fun updateUserIncome(income: Long) {
        if (firebaseAuth.currentUser != null) {
            referenceDatabase.child(USER_FIREBASE_PATH).child(firebaseAuth.currentUser!!.uid)
                .child("totalIncome")
                .setValue(income)
        }
    }

    override suspend fun updateSavingGoal(savingGoal: SavingGoal) {
        if (firebaseAuth.currentUser != null) {
            referenceDatabase.child(USER_FIREBASE_PATH).child(firebaseAuth.currentUser!!.uid)
                .child("savingGoal")
                .setValue(savingGoal)
        }
    }


    override suspend fun login(email: String, password: String): FireBaseState<String> {
        return try {
            Log.d(TAG, "login: before login success")
            firebaseAuth.signInWithEmailAndPassword(email, password).await()
            Log.d(TAG, "login: after login success")
            FireBaseState.Success("")
            
        } catch (exception: FirebaseException) {
            Log.d(TAG, "login: can't login")
            return FireBaseState.Fail(exception.message!!)
        }
    }

    override fun getCurrentUser(): Flow<User> {
        val userReference =
            referenceDatabase.child(USER_FIREBASE_PATH).child(firebaseAuth.currentUser!!.uid)

        return callbackFlow { 
            val postListener = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    // Get Post object and use the values to update the UI
                    val userFirebaseLogin = dataSnapshot.getValue(User::class.java)
                    try {
                        trySend(userFirebaseLogin!!)
                    } catch (exception: Exception) {
                        Log.d("Thao", "onDataChange: , line=137: GET ERROR when trySend USER FIRE BASE LOGIN")
                    }

                    Log.d("Thao", "UserDataSourceImpl: getCurrentUser: , line=94 $userFirebaseLogin")
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    // Getting Post failed, log a message
                }
            }

            userReference.addValueEventListener(postListener)

            awaitClose {
                userReference.removeEventListener(postListener)
            }
        }
    }


    companion object {
        private const val TAG = "UserDataSourceImpl"
    }
}