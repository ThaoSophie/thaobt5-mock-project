package fhn.thaobt5.personalfinancetracker.data.repository

import fhn.thaobt5.personalfinancetracker.data.mapper.SavingGoalMapper
import fhn.thaobt5.personalfinancetracker.data.mapper.TransactionMapper
import fhn.thaobt5.personalfinancetracker.data.network.firebase.SavingGoalDataSource
import fhn.thaobt5.personalfinancetracker.data.network.firebase.TransactionDataSource
import fhn.thaobt5.personalfinancetracker.data.network.firebase.UserDataSource
import fhn.thaobt5.personalfinancetracker.domain.models.SavingGoal
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.models.User
import fhn.thaobt5.personalfinancetracker.domain.repository.UserRepository
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class UserRepositoryFirebaseImpl @Inject constructor(
    private val userDataSource: UserDataSource,
    private val transactionDataSource: TransactionDataSource,
    private val savingGoalDataSource: SavingGoalDataSource
) : UserRepository {
    override suspend fun login(email: String, password: String): FireBaseState<String> {
        return userDataSource.login(email, password)
    }

    override suspend fun signUp(user: User): FireBaseState<String> {
        return userDataSource.signUp(user)
    }

    override fun logout(): FireBaseState<String> {
        return userDataSource.logout()
    }

    override suspend fun updateUserBalance(balance: Long) {
        return userDataSource.updateUserBalance(balance)
    }

    override suspend fun updateUserExpense(expense: Long) {
        return userDataSource.updateUserExpense(expense)
    }

    override suspend fun updateUserIncome(income: Long) {
        return userDataSource.updateUserIncome(income)
    }

    override suspend fun addATransaction(transaction: Transaction): FireBaseState<String> {
        return transactionDataSource.addTransaction(transaction)
    }

    override suspend fun updateTransaction(transaction: Transaction): FireBaseState<String> {
        return transactionDataSource.updateTransaction(transaction)
    }

    override suspend fun deleteTransaction(transaction: Transaction): FireBaseState<String> {
        return transactionDataSource.deleteTransaction(transaction)
    }

    override suspend fun getAllTransactionsByCurrentUser(): Flow<List<Transaction>> {
        return transactionDataSource.getUserTransactions().map {
            TransactionMapper.mapFromFirebaseModel(it)
        }
    }

    override fun getSavingGoalByCurrentUserFireBase(): Flow<List<SavingGoal>> {
        return savingGoalDataSource.getAllSavingGoals().map {
            SavingGoalMapper.mapFromFirebaseModel(it)
        }
    }

    override suspend fun addSavingGoalFireBase(savingGoal: SavingGoal): FireBaseState<String> {
        return if (savingGoalDataSource.addSavingGoal(SavingGoalMapper.mapFromModelToFirebaseModel(savingGoal))) {
            FireBaseState.Success(null)
        } else {
            FireBaseState.Fail("Add saving goal failed")
        }
    }


    override fun getCurrentUser(): Flow<User> {
        return userDataSource.getCurrentUser()
    }


    companion object {
        private const val TAG = "THAO UserRepositoryImpl"
    }
}