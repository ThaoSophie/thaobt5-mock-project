package fhn.thaobt5.personalfinancetracker.data.repository

import fhn.thaobt5.personalfinancetracker.data.models.api_model.CurrencyResponse
import fhn.thaobt5.personalfinancetracker.data.network.api.CurrencyApi
import fhn.thaobt5.personalfinancetracker.domain.repository.CurrencyApiRepository
import fhn.thaobt5.personalfinancetracker.utils.api_state.ApiResource
import javax.inject.Inject

class CurrencyApiRepositoryImpl @Inject constructor(
    private val api: CurrencyApi
): CurrencyApiRepository {
    override suspend fun getRates(base: String): ApiResource<CurrencyResponse> {
        return try {
            val response = api.getRates(base)
            val result = response.body()
            if(response.isSuccessful && result != null) {
                ApiResource.Success(result)
            } else {
                ApiResource.Error(response.message())
            }
        } catch (e: java.lang.Exception) {
            ApiResource.Error(e.message ?: "An error occured")
        }
    }
}