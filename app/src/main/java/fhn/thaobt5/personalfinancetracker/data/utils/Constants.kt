package fhn.thaobt5.personalfinancetracker.data.utils



const val USER_TABLE = "user_table"
const val TRANSACTION_TABLE = "transaction_table"

const val USER_FIREBASE_PATH = "users"
const val TRANSACTION_FIREBASE_PATH = "transactions"
const val SAVING_GOAL_PATH = "savingGoals"
const val AMOUNT_PATH = "amount"
const val DATE_PATH = "date"
const val NOTE_PATH = "note"
const val TAG_PATH = "tag"
const val TITLE_PATH = "title"
const val TRANSACTION_TYPE_PATH = "transactionType"
