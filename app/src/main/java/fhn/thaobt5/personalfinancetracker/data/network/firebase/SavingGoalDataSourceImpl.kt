package fhn.thaobt5.personalfinancetracker.data.network.firebase

import android.util.Log
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import fhn.thaobt5.personalfinancetracker.data.models.FireBaseSavingGoalModel
import fhn.thaobt5.personalfinancetracker.data.models.FireBaseTransactionModel
import fhn.thaobt5.personalfinancetracker.data.utils.SAVING_GOAL_PATH
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class SavingGoalDataSourceImpl @Inject constructor(
    private val auth: FirebaseAuth,
    private val databaseReference: DatabaseReference
): SavingGoalDataSource {

    override suspend fun addSavingGoal(fireBaseSavingGoalModel: FireBaseSavingGoalModel): Boolean {
        return try {
            val savingGoalRef = databaseReference.child(SAVING_GOAL_PATH).child(auth.currentUser!!.uid).push()
            fireBaseSavingGoalModel.userId = savingGoalRef.key.toString()
            savingGoalRef.setValue(fireBaseSavingGoalModel).await()
            true
        } catch (ex: FirebaseException) {
            false
        }
    }

    override fun getAllSavingGoals(): Flow<List<FireBaseSavingGoalModel>> {
        val savingGoalRef = databaseReference.child(SAVING_GOAL_PATH).child(auth.currentUser!!.uid)
        val savingGoalList = arrayListOf<FireBaseSavingGoalModel>()
        return callbackFlow {
            val valueEventListener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    savingGoalList.clear()
                    for (news in snapshot.children) {
                        news.getValue(FireBaseSavingGoalModel::class.java)
                            ?.let { savingGoalList.add(it) }
                    }
                    Log.d("Thao", "onDataChange: , line=46")
                    trySend(savingGoalList)
                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

            }

            savingGoalRef.addValueEventListener(valueEventListener)
            awaitClose { savingGoalRef.removeEventListener(valueEventListener) }
        }
    }
}