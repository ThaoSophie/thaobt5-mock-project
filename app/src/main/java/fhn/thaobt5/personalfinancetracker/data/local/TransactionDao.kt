package fhn.thaobt5.personalfinancetracker.data.local

import androidx.room.*
import fhn.thaobt5.personalfinancetracker.data.utils.TRANSACTION_TABLE
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.utils.TransactionType
import kotlinx.coroutines.flow.Flow


@Dao
interface TransactionDao {

    // insert new transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTransaction(transaction: Transaction)


    // update existing transaction
    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateTransaction(transaction: Transaction)

    // delete transaction
    @Delete
    suspend fun deleteTransaction(transaction: Transaction)

    // get all saved transaction list
    @Query("SELECT * FROM $TRANSACTION_TABLE ORDER by createdAt DESC")
    fun getAllTransactions(): Flow<List<Transaction>>


    // get all income or expense list by transaction type param
    @Query("SELECT * FROM $TRANSACTION_TABLE WHERE transactionType == :transactionType AND userId == :userID ORDER by createdAt DESC")
    fun getTransactionByType(transactionType: TransactionType, userID: String): Flow<List<Transaction>>


    // get all transaction by userId
    @Query("SELECT * FROM $TRANSACTION_TABLE WHERE userId == :userId ORDER by createdAt DESC")
    fun getTransactionsByUserID(userId: String): Flow<List<Transaction>>

    // get single transaction by tranId
    @Query("SELECT * FROM $TRANSACTION_TABLE WHERE tranId = :tranId")
    fun getTransactionByTranID(tranId: Int): Flow<Transaction>


    // delete transaction by id
    @Query("DELETE FROM $TRANSACTION_TABLE WHERE tranId = :tranId")
    suspend fun deleteTransactionByID(tranId: Int)

}