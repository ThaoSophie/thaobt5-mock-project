package fhn.thaobt5.personalfinancetracker.data.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class NetworkControl @Inject constructor(
    @ApplicationContext private val context: Context
) {

    fun isConnected(): Boolean {

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> {
                val network = connectivityManager.activeNetwork ?: return  false
                val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false
                activeNetwork.run {
                    when {
                        hasTransport(NetworkCapabilities.TRANSPORT_WIFI_AWARE) -> true
                        hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                        hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                        else -> false
                    }
                }

            }
            else -> {
                false
            }
        }

    }


}