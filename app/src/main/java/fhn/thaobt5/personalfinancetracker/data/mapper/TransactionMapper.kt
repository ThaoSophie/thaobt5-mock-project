package fhn.thaobt5.personalfinancetracker.data.mapper

import fhn.thaobt5.personalfinancetracker.data.models.FireBaseTransactionModel
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction

object TransactionMapper {

    fun mapFromFirebaseModel(source: List<FireBaseTransactionModel>): List<Transaction> {
        return source.map {
            Transaction(
                userId = it.userId!!,
                title = it.title!!,
                amount = it.amount!!,
                transactionType = it.transactionType,
                tag = it.tag!!,
                date = it.date!!,
                note = it.note!!,
                tranId = it.tranId!!,
                createdAt = it.createdAt!!
            )
        }
    }
}