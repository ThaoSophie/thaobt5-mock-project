package fhn.thaobt5.personalfinancetracker.presentation.fragments.transaction.details

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import fhn.thaobt5.personalfinancetracker.databinding.FragmentTransactionDetailsBinding
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.presentation.base.BaseFragment
import fhn.thaobt5.personalfinancetracker.presentation.viewmodels.TransactionViewModel
import fhn.thaobt5.personalfinancetracker.utils.safeNavigate
import fhn.thaobt5.personalfinancetracker.utils.viewState.DetailState
import kotlinx.coroutines.launch

@AndroidEntryPoint
class TransactionDetailsFragment :
    BaseFragment<FragmentTransactionDetailsBinding>(FragmentTransactionDetailsBinding::inflate) {

    private val args by navArgs<TransactionDetailsFragmentArgs>()

    private val transactionViewModel: TransactionViewModel by viewModels()

    override fun bindView() {
        super.bindView()
//        updateUI()
        setEditTransactionClick()
    }

    override fun observeViewModel() {
        super.observeViewModel()
        val transaction = args.transaction
        getTransaction(transaction.tranId)
        observerTransaction()
    }

    private fun getTransaction(transactionId: Int) {
        transactionViewModel.getTranByTranIdFromRoomDB(transactionId)
    }

    private fun observerTransaction() = lifecycleScope.launch {
        transactionViewModel.detailState.collect {detailState ->
            when (detailState) {
                DetailState.Loading -> {
                }

                is DetailState.Success -> {
                    onDetailsLoaded(detailState.transaction)

                }

                is DetailState.Error -> {

                }

                DetailState.Empty -> {}

            }

        }
    }

    private fun onDetailsLoaded(transaction: Transaction) = with (binding.transactionDetails) {
        tvTitle.text = transaction.title
        tvAmount.text = transaction.amount.toString()
        tvType.text = transaction.transactionType!!.name
        tvTag.text = transaction.tag!!.type
        tvDate.text = transaction.date
        tvNote.text = transaction.note
        tvCreatedAt.text = transaction.createdAtDateFormat

        (activity as AppCompatActivity).supportActionBar!!.title = transaction.title

    }

    private fun setEditTransactionClick() {
        binding.editTransaction.setOnClickListener {
            findNavController().safeNavigate(
                TransactionDetailsFragmentDirections.actionTransactionDetailsFragmentToEditTransactionFragment(
                    transaction = args.transaction
                )
            )
        }
    }

    private fun updateUI() {
        with(binding.transactionDetails) {
            tvTitle.text = args.transaction.title
            tvAmount.text = args.transaction.amount.toString()
            tvType.text = args.transaction.transactionType!!.name
            tvTag.text = args.transaction.tag!!.type
            tvDate.text= args.transaction.date
            tvNote.text= args.transaction.note
            tvCreatedAt.text= args.transaction.createdAtDateFormat
        }

    }

    companion object {
        const val TAG = "Thao TransactionDetailFragment"
    }


}