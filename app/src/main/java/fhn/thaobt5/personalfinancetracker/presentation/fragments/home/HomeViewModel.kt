package fhn.thaobt5.personalfinancetracker.presentation.fragments.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.firebase_usecase.AddTransactionToFireBase
import fhn.thaobt5.personalfinancetracker.domain.usecases.user_usecase.UpdateUserInfoUseCase
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val updateUserInfoUseCase: UpdateUserInfoUseCase,
    private val addTransactionToFireBase: AddTransactionToFireBase
): ViewModel() {

    private val _addTransactionState = MutableStateFlow<FireBaseState<String>>(FireBaseState.Loading(""))
    val addTransactionState: StateFlow<FireBaseState<String>> get() = _addTransactionState

    private val _userBalance = MutableStateFlow<FireBaseState<String>>(FireBaseState.Loading(""))
    val userBalance: StateFlow<FireBaseState<String>> get() = _userBalance


    fun addTransaction(transaction: Transaction) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _addTransactionState.update {
                    addTransactionToFireBase(transaction)
                }
            }
        }
    }


}