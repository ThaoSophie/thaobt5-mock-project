package fhn.thaobt5.personalfinancetracker.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import fhn.thaobt5.personalfinancetracker.R
import fhn.thaobt5.personalfinancetracker.databinding.ItemTransactionLayoutBinding
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.utils.TransactionTag
import fhn.thaobt5.personalfinancetracker.domain.utils.TransactionType
import fhn.thaobt5.personalfinancetracker.utils.vietNamDong

class TransactionAdapter: RecyclerView.Adapter<TransactionAdapter.ViewHolder>() {
    inner class ViewHolder(val binding: ItemTransactionLayoutBinding):
        RecyclerView.ViewHolder(binding.root)

    private val differCallback = object : DiffUtil.ItemCallback<Transaction>() {
        override fun areItemsTheSame(oldItem: Transaction, newItem: Transaction): Boolean {
            return oldItem.tranId == newItem.tranId
        }

        override fun areContentsTheSame(oldItem: Transaction, newItem: Transaction): Boolean {
            return oldItem == newItem
        }

    }

    val differ = AsyncListDiffer(this, differCallback)
    // on item click listener
    private var onItemClickListener: ((Transaction) -> Unit)? = null
    fun setOnItemClickListener(listener: (Transaction) -> Unit) {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemTransactionLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = differ.currentList[position]

        holder.binding.apply {
            tvTransactionName.text = item.title
            tvTransactionCategory.text = item.tag!!.type
            tvCreatedAt.text = item.date

            when (item.transactionType) {
                TransactionType.INCOME -> {
                    tvTransactionAmount.setTextColor(
                        ContextCompat.getColor(
                            tvTransactionAmount.context,
                            R.color.income
                        )

                    )

                    tvTransactionAmount.text = "+ ".plus(vietNamDong(item.amount))

                }

                TransactionType.EXPENSE -> {
                    tvTransactionAmount.setTextColor(
                        ContextCompat.getColor(
                            tvTransactionAmount.context,
                            R.color.expense
                        )
                    )

                    tvTransactionAmount.text = "- ".plus(vietNamDong(item.amount))

                }

                TransactionType.OVERALL -> {

                }
                else -> {}
            }

            when (item.tag) {
                TransactionTag.HOUSING -> {
                    imageViewTransactionIcon.setImageResource(R.drawable.ic_housing)
                }

                TransactionTag.TRANSPORTATION -> {
                    imageViewTransactionIcon.setImageResource(R.drawable.ic_transport)
                }

                TransactionTag.FOOD -> {
                    imageViewTransactionIcon.setImageResource(R.drawable.ic_food)
                }

                TransactionTag.UTILITIES -> {
                    imageViewTransactionIcon.setImageResource(R.drawable.ic_utilities)
                }

                TransactionTag.INSURANCE -> {
                    imageViewTransactionIcon.setImageResource(R.drawable.ic_insurance)
                }

                TransactionTag.HEALTHCARE -> {
                    imageViewTransactionIcon.setImageResource(R.drawable.ic_medical)
                }

                TransactionTag.SAVING_DEBTS -> {
                    imageViewTransactionIcon.setImageResource(R.drawable.ic_savings)
                }

                TransactionTag.PERSONAL_SPENDING -> {
                    imageViewTransactionIcon.setImageResource(R.drawable.ic_personal_spending)
                }

                TransactionTag.ENTERTAINMENT -> {
                    imageViewTransactionIcon.setImageResource(R.drawable.ic_entertainment)
                }

                TransactionTag.OTHERS -> {
                    imageViewTransactionIcon.setImageResource(R.drawable.ic_others)
                }

                else -> {}


            }

            // on item click
            holder.itemView.setOnClickListener {
                onItemClickListener?.let { it(item) }
            }
        }
    }


}