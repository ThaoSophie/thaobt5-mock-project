package fhn.thaobt5.personalfinancetracker.presentation.fragments.transaction.edit


import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import android.view.View.OnClickListener
import android.widget.ArrayAdapter
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.internal.ViewUtils
import dagger.hilt.android.AndroidEntryPoint
import fhn.thaobt5.personalfinancetracker.R
import fhn.thaobt5.personalfinancetracker.databinding.FragmentEditTransactionBinding
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.utils.TransactionTag
import fhn.thaobt5.personalfinancetracker.domain.utils.TransactionType
import fhn.thaobt5.personalfinancetracker.presentation.base.BaseFragment
import fhn.thaobt5.personalfinancetracker.presentation.fragments.transaction.add.AddTransactionFragment
import fhn.thaobt5.personalfinancetracker.presentation.viewmodels.MainViewModel
import fhn.thaobt5.personalfinancetracker.presentation.viewmodels.TransactionViewModel
import fhn.thaobt5.personalfinancetracker.utils.Constants
import fhn.thaobt5.personalfinancetracker.utils.parseDouble
import fhn.thaobt5.personalfinancetracker.utils.snack
import fhn.thaobt5.personalfinancetracker.utils.transformIntoDatePicker
import fhn.thaobt5.personalfinancetracker.utils.viewState.ViewState
import kotlinx.coroutines.launch
import java.util.*

@AndroidEntryPoint
class EditTransactionFragment :
    BaseFragment<FragmentEditTransactionBinding>(FragmentEditTransactionBinding::inflate),
    OnClickListener
{

    private val args by navArgs<EditTransactionFragmentArgs>()
    private var currentUserID: String? = null
    private val mainViewModel: MainViewModel by activityViewModels()
    private val transactionViewModel: TransactionViewModel by viewModels()

    companion object {
        const val TAG = "Thao EditTransactionFragment"
    }

    override fun observeViewModel() {
        super.observeViewModel()

        mainViewModel.userData.observe(viewLifecycleOwner) {
            currentUserID = it.userId
        }

        updateTranDataToFireBase()
    }

    @SuppressLint("LongLogTag")
    private fun updateTranDataToFireBase() = lifecycleScope.launch {
        transactionViewModel.uiState.collect {uiState ->
            when (uiState) {
                is ViewState.Success -> {
                    transactionViewModel.updateTransactionToFireBase(getTransactionContent())
                    transactionViewModel.updateUserInforToFireBase(uiState.transactions)

                    Log.d("Thao EditTransactionFragment", "updateTranDataToFireBase: , line=62")
                }
                else -> {}
            }

        }

    }

    @SuppressLint("RestrictedApi")
    override fun bindView() {
        super.bindView()
        initViews()
        updateTransactionInfo()

        binding.constraintLayout.setOnClickListener {
            ViewUtils.hideKeyboard(it)
        }

    }




    private fun initViews() = with(binding) {
        val transactionTypeAdapter = ArrayAdapter(
            requireContext(),
            R.layout.item_autocomplete_layout,
            Constants.transactionTypes
        )

        val tagsAdapter = ArrayAdapter(
            requireContext(),
            R.layout.item_autocomplete_layout,
            Constants.transactionTags
        )

        // Set list to TextInputEditText adapter
        addTransactionLayout.etTransactionType.setAdapter(transactionTypeAdapter)
        addTransactionLayout.etTag.setAdapter(tagsAdapter)

        // Transform TextInputEditText to DatePicker using Ext function
        addTransactionLayout.etWhen.transformIntoDatePicker(
            requireContext(),
            "dd/MM/yyyy",
            Date()
        )

        btnSaveTransaction.setOnClickListener(this@EditTransactionFragment)
    }

    private fun getTransactionContent(): Transaction = binding.addTransactionLayout.let {
        val transactionId = args.transaction.tranId
        val title = it.etTitle.text.toString()
        val amount = parseDouble(it.etAmount.text.toString())

        val tranType: TransactionType? = try {
            TransactionType.valueOf(it.etTransactionType.text.toString())
        } catch (ex: Exception) {
            null
        }

        val tag: TransactionTag? = try {
            TransactionTag.valueOf(it.etTag.text.toString())
        } catch (ex: Exception) {
            null
        }

        val date = it.etWhen.text.toString()
        val note = it.etNote.text.toString()

        return Transaction(
            tranId = transactionId,
            title = title,
            userId = currentUserID!!,
            amount = amount,
            transactionType = tranType,
            tag = tag,
            date = date,
            note = note
        )
    }

    private fun updateTransactionInfo() {
        with(binding.addTransactionLayout) {
            with (args) {
                etTitle.setText(transaction.title)
                etAmount.setText(transaction.amount.toString())
                etTransactionType.setText(transaction.transactionType!!.name)
                etTag.setText(transaction.tag!!.name)
                etWhen.setText(transaction.date)
                etNote.setText(transaction.note)

            }
        }

    }

    @SuppressLint("RestrictedApi")
    override fun onClick(view: View?) {
        with (binding) {
            when (view) {
                btnSaveTransaction -> {
                    val (userId, title, amount, tranType, tag, date, note) = getTransactionContent()

                    addTransactionLayout.apply {
                        // validate if transaction content is empty or not
                        when {
                            title.isEmpty() -> {
                                this.etTitle.error = "Title must not be empty"
                            }

                            amount.isNaN() -> {
                                this.etAmount.error = "Amount must not be empty"
                            }

                            tranType == TransactionType.OVERALL -> {
                                this.etTransactionType.error = "Transaction type must not be empty"
                            }

                            tag == null -> {
                                this.etTag.error = "Tag must not be empty"
                            }

                            date.isEmpty() -> {
                                this.etWhen.error = "Date must not be empty"
                            }

                            note.isEmpty() -> {
                                this.etNote.error = "Note must not be empty"
                            }
                            else -> {
                                transactionViewModel.updateTransactionToRoomDB(getTransactionContent()).also {
                                    transactionViewModel.getAllTransactionFromRoomDB(getTransactionContent().userId)
                                    binding.root.snack(
                                        string = R.string.success_transaction_saved
                                    ).run {
                                        findNavController().popBackStack()
                                    }
                                }



                            }


                        }
                    }




                }
                constraintLayout -> {
                    ViewUtils.hideKeyboard(view)
                }
                else -> {}
            }
        }

    }



}