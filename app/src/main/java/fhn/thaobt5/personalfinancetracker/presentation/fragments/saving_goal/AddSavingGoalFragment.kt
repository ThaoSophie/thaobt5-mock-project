package fhn.thaobt5.personalfinancetracker.presentation.fragments.saving_goal

import android.annotation.SuppressLint
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.internal.ViewUtils
import dagger.hilt.android.AndroidEntryPoint
import fhn.thaobt5.personalfinancetracker.R
import fhn.thaobt5.personalfinancetracker.databinding.FragmentAddSavingGoalBinding
import fhn.thaobt5.personalfinancetracker.domain.models.SavingGoal
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import fhn.thaobt5.personalfinancetracker.presentation.base.BaseFragment
import fhn.thaobt5.personalfinancetracker.presentation.viewmodels.MainViewModel
import fhn.thaobt5.personalfinancetracker.utils.parseDouble
import fhn.thaobt5.personalfinancetracker.utils.snack
import fhn.thaobt5.personalfinancetracker.utils.transformIntoDatePicker
import java.util.*


@AndroidEntryPoint
class AddSavingGoalFragment :
    BaseFragment<FragmentAddSavingGoalBinding>(FragmentAddSavingGoalBinding::inflate),
    View.OnClickListener {

    private val savingGoalsViewModel: SavingGoalsViewModel by viewModels()

    override fun observeViewModel() {
        super.observeViewModel()

        savingGoalsViewModel.addSavingGoalState.observe(viewLifecycleOwner) { addSavingGoalState ->
            when (addSavingGoalState) {
                is FireBaseState.Success -> {
                    binding.root.snack(string = R.string.success_saving_goal_saved)
                    findNavController().popBackStack()

                }
                else -> {}
            }

        }
    }

    @SuppressLint("RestrictedApi")
    override fun bindView() {
        super.bindView()

        initViews()

        binding.constraintLayout.setOnClickListener {
            ViewUtils.hideKeyboard(it)
        }
    }

    private fun initViews() {
        with(binding) {


            // Transform TextInputEditText to DatePicker using Ext function
            addSavingGoalLayout.etStartDate.transformIntoDatePicker(
                requireContext(),
                "dd/MM/yyyy",
                Date()
            )

            addSavingGoalLayout.etExpiryDate.transformIntoDatePicker(
                requireContext(),
                "dd/MM/yyyy",
                Date()
            )

            btnSaveGoal.setOnClickListener(this@AddSavingGoalFragment)
        }
    }

    private fun getSavingGoalContent(): SavingGoal = binding.addSavingGoalLayout.let {
        val title = it.etTitle.text.toString()
        val amount = parseDouble(it.etAmount.text.toString())
        val startDate = it.etStartDate.text.toString()
        val expiryDate = it.etExpiryDate.text.toString()
        return SavingGoal(title, amount, startDate, expiryDate)
    }

    override fun onClick(view: View?) {
        val savingGoal = getSavingGoalContent()
        with(binding) {
            when (view) {
                binding.btnSaveGoal -> {
                    addSavingGoalLayout.apply {
                        // validate if saving goal content is empty or not
                        when {
                            savingGoal.title!!.isEmpty() -> {
                                this.etTitle.error = "Title must not be empty"
                            }

                            savingGoal.amount!!.isNaN() -> {
                                this.etAmount.error = "Amount must not be empty"

                            }

                            savingGoal.startDate!!.isEmpty() -> {
                                this.etStartDate.error = "Start date must not be empty"

                            }

                            savingGoal.endDate!!.isEmpty() -> {
                                this.etExpiryDate.error = "Expiry date must not be empty"

                            }

                            else -> {
                                savingGoalsViewModel.addSavingGoalFireBase(savingGoal)
//                                    .run {
//                                    binding.root.snack(
//                                        string = R.string.success_saving_goal_saved
//                                    )
                            }
                        }
                    }
                }

                else -> {}
            }

        }
    }


}