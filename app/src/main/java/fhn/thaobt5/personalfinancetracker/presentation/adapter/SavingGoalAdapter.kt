package fhn.thaobt5.personalfinancetracker.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import fhn.thaobt5.personalfinancetracker.databinding.ItemSavingGoalLayoutBinding
import fhn.thaobt5.personalfinancetracker.domain.models.SavingGoal
import fhn.thaobt5.personalfinancetracker.utils.vietNamDong

class SavingGoalAdapter: RecyclerView.Adapter<SavingGoalAdapter.ViewHolder>(){

    inner class ViewHolder(val binding: ItemSavingGoalLayoutBinding):
        RecyclerView.ViewHolder(binding.root)

    private var list: List<SavingGoal> = listOf()
    private var onItemClickListener: ((SavingGoal) -> Unit)? = null


    fun setOnItemClickListener(listener: (SavingGoal) -> Unit) {
        onItemClickListener = listener
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemSavingGoalLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.binding.apply {
            tvTitle.text = item.title
            tvAmount.text = vietNamDong(item.amount!!)

            tvExpiryDate.text = item.endDate
        }

        holder.binding.root.setOnClickListener {
            onItemClickListener?.let {
                it(item)
            }
        }
    }

    fun submitList(savingGoalList: List<SavingGoal>) {
        list = savingGoalList
        notifyDataSetChanged()
    }
}