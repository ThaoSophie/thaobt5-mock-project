package fhn.thaobt5.personalfinancetracker.presentation.fragments.saving_goal

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fhn.thaobt5.personalfinancetracker.domain.models.SavingGoal
import fhn.thaobt5.personalfinancetracker.domain.usecases.saving_usecases.AddSavingGoalFireBaseUseCase
import fhn.thaobt5.personalfinancetracker.domain.usecases.saving_usecases.GetAllSavingGoalFireBaseUseCase
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import fhn.thaobt5.personalfinancetracker.utils.DispatcherProvider
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SavingGoalsViewModel @Inject constructor(
    private val getAllSavingGoalFireBaseUseCase: GetAllSavingGoalFireBaseUseCase,
    private val addSavingGoalFireBaseUseCase: AddSavingGoalFireBaseUseCase,
    private val dispatcher: DispatcherProvider
) : ViewModel() {

    private val _savingGoalList = MutableLiveData<List<SavingGoal>>()
    val savingGoalList: LiveData<List<SavingGoal>> get() = _savingGoalList

    private val _addSavingGoalState = MutableLiveData<FireBaseState<String>?>()
    val addSavingGoalState: LiveData<FireBaseState<String>?> get() = _addSavingGoalState

    fun getAllSavingGoalFireBase() {
        viewModelScope.launch(dispatcher.io) {
            getAllSavingGoalFireBaseUseCase().collect() {
                _savingGoalList.postValue(it)
            }

        }
    }

    fun addSavingGoalFireBase(savingGoal: SavingGoal) {
        viewModelScope.launch(dispatcher.io) {
            _addSavingGoalState.postValue(
                addSavingGoalFireBaseUseCase(savingGoal)
            )
        }
    }
}