package fhn.thaobt5.personalfinancetracker.presentation.activity

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import dagger.hilt.android.AndroidEntryPoint
import fhn.thaobt5.personalfinancetracker.R
import fhn.thaobt5.personalfinancetracker.databinding.ActivityMainBinding
import fhn.thaobt5.personalfinancetracker.presentation.viewmodels.MainViewModel
import fhn.thaobt5.personalfinancetracker.utils.hide
import fhn.thaobt5.personalfinancetracker.utils.show

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var navHostFragment: NavHostFragment
    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    private lateinit var binding: ActivityMainBinding

    private val mainViewModel: MainViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initViews(binding)
        setUpToolBarAndBNV(binding, navHostFragment.navController)

        binding.bottomNavigationView.setOnItemReselectedListener { item ->
            val reSelectedDestinationId = item.itemId
            navController.popBackStack(reSelectedDestinationId, inclusive = false)

        }
    }



    private fun initViews(binding: ActivityMainBinding) {
        setSupportActionBar(binding.toolbar)
        supportActionBar!!.setDisplayShowCustomEnabled(false)

        navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment?
            ?: return
        navController = navHostFragment.navController

        // set up bottom navigation view with navController
        with(binding) {
            bottomNavigationView.background = null
            bottomNavigationView.setupWithNavController(navController)
        }


        // Set up tool bar with navController and appBarConfiguration
        with(navController) {
            appBarConfiguration = AppBarConfiguration(graph)
            setupActionBarWithNavController(this, appBarConfiguration)
        }

    }


    private fun setUpToolBarAndBNV(binding: ActivityMainBinding, navController: NavController) {
        navController.addOnDestinationChangedListener {_, destination, _ ->
            when (destination.id) {

                R.id.loginFragment -> {
                    showToolBarAndBNV(
                        true,
                        false,
                        getString(R.string.login)

                    )
                }

                R.id.splashFragment -> {
                    showToolBarAndBNV(
                        true,
                        false,
                        getString(R.string.money_lover)

                    )
                }

                R.id.signUpFragment -> {
                    showToolBarAndBNV(
                        true,
                        false,
                        getString(R.string.sign_up)

                    )
                }

                R.id.profileFragment -> {
                    showToolBarAndBNV(
                        true,
                        true,
                        "Profile"
                    )
                }

                R.id.homeFragment -> {
                    showToolBarAndBNV(
                        false,
                        true
                    )
                }

                R.id.addTransactionFragment -> {
                    showToolBarAndBNV(
                        true,
                        true,
                        "Add Transaction"
                    )
                }

                R.id.transactionDetailsFragment -> {
                    showToolBarAndBNV(
                        true,
                        false,
                        "Transaction Detail"
                    )
                }

                R.id.editTransactionFragment -> {
                    showToolBarAndBNV(
                        true,
                        false,
                        "Edit Transaction"
                    )
                }

                R.id.allTransactionFragment -> {
                    showToolBarAndBNV(
                        false,
                        true,
                    )
                }


                R.id.savingGoalsFragment -> {
                    showToolBarAndBNV(
                        true,
                        true,
                        "Saving Account"
                    )
                }

                R.id.addSavingGoalFragment -> {
                    showToolBarAndBNV(
                        true,
                        false,
                        "Add Goal"

                    )
                }

                R.id.savingGoalDetailFragment -> {
                    showToolBarAndBNV(
                        true,
                        false,
                        "Saving Goal Details"
                    )
                }
            }
        }



    }

    private fun showToolBarAndBNV(visibleAB: Boolean, visibleBNV: Boolean, toolbarTitle: String = "") {
        with (binding) {
            if (visibleAB) appBarLayout.show() else appBarLayout.hide()
//            appBarLayout.isVisible = visibleAB
            if (visibleBNV) bottomNavigationView.show() else bottomNavigationView.hide()
            toolbar.title= toolbarTitle
        }
    }




    override fun onSupportNavigateUp(): Boolean {
        navHostFragment.navController.navigateUp()
        return super.onSupportNavigateUp()
    }
}