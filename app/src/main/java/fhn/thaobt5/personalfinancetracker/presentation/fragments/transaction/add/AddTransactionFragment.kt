package fhn.thaobt5.personalfinancetracker.presentation.fragments.transaction.add

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.android.material.internal.ViewUtils
import dagger.hilt.android.AndroidEntryPoint
import fhn.thaobt5.personalfinancetracker.R
import fhn.thaobt5.personalfinancetracker.data.utils.NetworkControl
import fhn.thaobt5.personalfinancetracker.databinding.FragmentAddTransactionBinding
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.utils.TransactionTag
import fhn.thaobt5.personalfinancetracker.domain.utils.TransactionType
import fhn.thaobt5.personalfinancetracker.presentation.base.BaseFragment
import fhn.thaobt5.personalfinancetracker.presentation.viewmodels.MainViewModel
import fhn.thaobt5.personalfinancetracker.presentation.viewmodels.TransactionViewModel
import fhn.thaobt5.personalfinancetracker.utils.*
import fhn.thaobt5.personalfinancetracker.utils.viewState.ViewState
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject


@AndroidEntryPoint
class AddTransactionFragment :
    BaseFragment<FragmentAddTransactionBinding>(FragmentAddTransactionBinding::inflate), View.OnClickListener {

    private val tranViewModel: TransactionViewModel by viewModels()
    private val mainViewModel: MainViewModel by activityViewModels()

    private var currentUserID: String? = null

    @Inject
    lateinit var networkControl: NetworkControl

    @SuppressLint("LongLogTag")
    override fun observeViewModel() {
        super.observeViewModel()
        mainViewModel.userData.observe(viewLifecycleOwner) {
            currentUserID = it.userId
        }
        updateTranDataToFireBase()

    }

    @SuppressLint("LongLogTag")
    private fun updateTranDataToFireBase() = lifecycleScope.launch {
        tranViewModel.uiState.collect {uiState ->
            when (uiState) {
                is ViewState.Success -> {
                    tranViewModel.updateUserInforToFireBase(uiState.transactions)
                    tranViewModel.addTransactionToFireBase(uiState.transactions.first())
                    Log.d(TAG, "observeViewModel: update user transaction, balance, income, expense to fire base")
                }
                else -> {}
            }

        }

    }

    @SuppressLint("RestrictedApi")
    override fun bindView() {
        super.bindView()

        initViews()

        binding.constraintLayout.setOnClickListener {
            ViewUtils.hideKeyboard(it)
        }
    }

    private fun initViews() {
        val transactionTypeAdapter =
            ArrayAdapter(
                requireContext(),
                R.layout.item_autocomplete_layout,
                Constants.transactionTypes
            )

        val tagsAdapter =
            ArrayAdapter(
                requireContext(),
                R.layout.item_autocomplete_layout,
                Constants.transactionTags
            )

        with (binding) {

            // Set list to TextInputEditText adapter
            addTransactionLayout.etTransactionType.setAdapter(transactionTypeAdapter)
            addTransactionLayout.etTag.setAdapter(tagsAdapter)

            // Transform TextInputEditText to DatePicker using Ext function
            addTransactionLayout.etWhen.transformIntoDatePicker(
                requireContext(),
                "dd/MM/yyyy",
                Date()
            )

            btnSaveTransaction.setOnClickListener(this@AddTransactionFragment)
        }
    }

    private fun getTransactionContent(): Transaction = binding.addTransactionLayout.let {
        val title = it.etTitle.text.toString()
        val amount = parseDouble(it.etAmount.text.toString())

        val tranType: TransactionType? = try {
            TransactionType.valueOf(it.etTransactionType.text.toString())
        } catch (ex: Exception) {
            null
        }

        val tag: TransactionTag? = try {
            TransactionTag.valueOf(it.etTag.text.toString())
        } catch (ex: Exception) {
            null
        }

        val date = it.etWhen.text.toString()
        val note = it.etNote.text.toString()

        return Transaction(
            title = title,
            userId = currentUserID!!,
            amount = amount,
            transactionType = tranType,
            tag = tag,
            date = date,
            note = note
        )
    }

    companion object {
        const val TAG = "Thao AddTransactionFragment"
    }

    @SuppressLint("LongLogTag")
    override fun onClick(view: View?) {
        with(binding) {
            when (view) {
                btnSaveTransaction -> {
                    val (userId, title, amount, tranType, tag, date, note) = getTransactionContent()

                    addTransactionLayout.apply {
                        // validate if transaction content is empty or not
                        when {
                            title.isEmpty() -> {
                                this.etTitle.error = "Title must not be empty"
                            }

                            amount.isNaN() -> {
                                this.etAmount.error = "Amount must not be empty"
                            }

                            tranType == TransactionType.OVERALL -> {
                                this.etTransactionType.error = "Transaction type must not be empty"
                            }

                            tag == null -> {
                                this.etTag.error = "Tag must not be empty"
                            }

                            date.isEmpty() -> {
                                this.etWhen.error = "Date must not be empty"
                            }

                            note.isEmpty() -> {
                                this.etNote.error = "Note must not be empty"
                            }

                            else -> {
                                tranViewModel.insertTransactionToRoomDB(getTransactionContent()).run {
//                                    tranViewModel.overall()
//                                    tranViewModel.getTransactionsByTypeFromRoomDB(
//                                        TransactionType.OVERALL,
//                                        userId
//                                    )
                                    tranViewModel.getAllTransactionFromRoomDB(userId)

                                    binding.root.snack(
                                        string = R.string.success_transaction_saved
                                    )

                                    findNavController().navigate(
                                        AddTransactionFragmentDirections.actionAddTransactionFragmentToHomeFragment2()
                                    )

                                }

                            }
                        }
                    }



                }
                else -> {}
            }

        }

    }
}
