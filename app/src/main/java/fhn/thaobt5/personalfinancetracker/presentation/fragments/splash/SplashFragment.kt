package fhn.thaobt5.personalfinancetracker.presentation.fragments.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import fhn.thaobt5.personalfinancetracker.R
import fhn.thaobt5.personalfinancetracker.data.local.SharedPreferenceHelper
import fhn.thaobt5.personalfinancetracker.databinding.FragmentSplashBinding
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import fhn.thaobt5.personalfinancetracker.presentation.base.BaseFragment
import fhn.thaobt5.personalfinancetracker.presentation.viewmodels.MainViewModel
import fhn.thaobt5.personalfinancetracker.utils.safeNavigate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class SplashFragment : BaseFragment<FragmentSplashBinding>(FragmentSplashBinding::inflate) {

    @Inject
    lateinit var sharedPreferenceHelper: SharedPreferenceHelper

    private val mainViewModel: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return super.onCreateView(inflater, container, savedInstanceState)

    }

    override fun bindView() {
        super.bindView()
        setAnimation()
        checkLogin()
    }

    private fun setAnimation() {
        with (binding) {
            binding.logo.startAnimation(
                AnimationUtils.loadAnimation(
                    requireContext(),
                    R.anim.slide)
            )

            binding.tvAppName.startAnimation(
                AnimationUtils.loadAnimation(
                    requireContext(),
                    R.anim.slide
                )
            )
        }
    }

    private fun checkLogin() {
        lifecycleScope.launch {
            delay(1000L)
            if (sharedPreferenceHelper.getIsOpenFirstTime()) {
                findNavController().safeNavigate(
                    SplashFragmentDirections.actionSplashFragmentToLoginFragment()
                )
            }

            if (!sharedPreferenceHelper.getIsLoggedIn() && !sharedPreferenceHelper.getIsRemember()) {
                findNavController().safeNavigate(
                    SplashFragmentDirections.actionSplashFragmentToLoginFragment()
                )
            } else {
                mainViewModel.login(
                    sharedPreferenceHelper.getEmail()!!,
                    sharedPreferenceHelper.getPassword()!!,
                    sharedPreferenceHelper.getIsRemember()
                )
            }
        }
    }

    override fun observeViewModel() {
        super.observeViewModel()
        lifecycleScope.launch {
            mainViewModel.loginState.collectLatest {
                handleUserLogin(it)

            }
        }
    }

    private fun handleUserLogin(state: FireBaseState<String>) {
        when (state) {
            is FireBaseState.Success -> {
                if (sharedPreferenceHelper.getIsLoggedIn()) {
                    mainViewModel.getProfile()
                    findNavController().safeNavigate(
                        SplashFragmentDirections.actionSplashFragmentToHomeFragment()
                    )
                }
            }

            else -> {

            }
        }

    }

    companion object {
        private const val TAG = "THAO SplashFragment"
    }



}