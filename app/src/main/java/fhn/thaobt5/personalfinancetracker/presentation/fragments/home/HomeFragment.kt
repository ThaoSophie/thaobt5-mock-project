package fhn.thaobt5.personalfinancetracker.presentation.fragments.home

import android.annotation.SuppressLint
import android.graphics.Color
import android.util.Log
import android.view.*
import android.view.View.OnClickListener
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.internal.ViewUtils
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import fhn.thaobt5.personalfinancetracker.R
import fhn.thaobt5.personalfinancetracker.databinding.FragmentHomeBinding
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.models.User
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import fhn.thaobt5.personalfinancetracker.domain.utils.TransactionType
import fhn.thaobt5.personalfinancetracker.presentation.adapter.TransactionAdapter
import fhn.thaobt5.personalfinancetracker.presentation.base.BaseFragment
import fhn.thaobt5.personalfinancetracker.presentation.viewmodels.MainViewModel
import fhn.thaobt5.personalfinancetracker.presentation.viewmodels.TransactionViewModel
import fhn.thaobt5.personalfinancetracker.utils.*
import fhn.thaobt5.personalfinancetracker.utils.api_state.CurrencyApiEvent
import fhn.thaobt5.personalfinancetracker.utils.viewState.ViewState
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate),
    OnClickListener {

    private val mainViewModel: MainViewModel by activityViewModels()
    private val transactionViewModel: TransactionViewModel by viewModels()

    private lateinit var transactionAdapter: TransactionAdapter
    private var totalIncome = 0.0
    private var totalExpense = 0.0
    private var adapterTranList = ArrayList<Transaction>()
    private var currentUserID: String? = null


    override fun observeViewModel() {
        super.observeViewModel()
        mainViewModel.getProfile()
        lifecycleScope.launch {
            mainViewModel.userData.observe(viewLifecycleOwner) {
                handleLoginResult(it)
                transactionViewModel.getTransactionsByTypeFromRoomDB(
                    TransactionType.OVERALL,
                    it.userId
                )
                currentUserID = it.userId
            }

        }

        observeConversion()

        setupRV()
        initViews()
        swipeToDelete()
        observeTransaction()

        updateUserInfoDataToFireBase()


    }

    private fun updateUserInfoDataToFireBase() = lifecycleScope.launch {
        transactionViewModel.uiState.collect { uiState ->
            when (uiState) {
                is ViewState.Success -> {
                    transactionViewModel.updateUserInforToFireBase(uiState.transactions)

                    Log.d("Thao HomeFragment", "updateUserInfoDataToFireBase: , line=77")
                }
                else -> {}
            }

        }

    }

    private fun swipeToDelete() {
        // init item touch callback for swipe action
        val itemTouchHelperCallback = object : ItemTouchHelper.SimpleCallback(
            ItemTouchHelper.UP or ItemTouchHelper.DOWN,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return true
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                // get item position & delete notes
                val position = viewHolder.adapterPosition
                val transactionItem = transactionAdapter.differ.currentList[position]


                transactionViewModel.deleteTransactionFromFireBase(transactionItem)
                transactionViewModel.deleteTransactionFromRoomDB(transactionItem)
                transactionViewModel.getAllTransactionFromRoomDB(currentUserID!!)
                transactionAdapter.notifyDataSetChanged()

                Snackbar.make(
                    binding.root,
                    getString(R.string.success_transaction_delete),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }

        ItemTouchHelper(itemTouchHelperCallback).apply {
            attachToRecyclerView(binding.rvTransaction)
        }
    }

    private fun observeConversion() {
        lifecycleScope.launch {
            mainViewModel.conversion.collect { currencyApiEvent ->
                with(binding) {
                    when (currencyApiEvent) {
                        is CurrencyApiEvent.Success -> {
                            progressBar.hide()
                            tvResult.setTextColor(Color.BLACK)
                            tvResult.text = currencyApiEvent.resultText
                        }

                        is CurrencyApiEvent.Failure -> {
                            progressBar.hide()
                            tvResult.setTextColor(Color.RED)
                            binding.tvResult.text = currencyApiEvent.errorText
                        }

                        is CurrencyApiEvent.Loading -> {
                            progressBar.show()
                        }
                        else -> Unit
                    }
                }
            }
        }
    }

    private fun handleLoginResult(user: User) {
        with(binding) {
            tvUserName.text = user.userName
//            totalBalanceView.tvTotalBalance.text = vietNamDong(user.balance.toDouble())
//            incomeCardView.tvTotalIncome.text = vietNamDong(user.totalIncome.toDouble())
//            expenseCardView.tvTotalExpense.text= vietNamDong(user.totalExpense.toDouble())
        }

        mainViewModel.getTransactionListFromFirebase()

        mainViewModel.transactionListFromFireBase.observe(viewLifecycleOwner) { tranList ->
            for (transaction in tranList) {
                transactionViewModel.insertTransactionToRoomDB(transaction)
            }

        }
    }

    override fun bindView() {
        super.bindView()
        Log.d(TAG, "bindView: ")
        pressAgainToExit()
    }

    private fun observeTransaction() = lifecycleScope.launch {
        transactionViewModel.uiState.collect { uiState ->
            when (uiState) {
                is ViewState.Loading -> {

                }

                is ViewState.Success -> {
                    showRvTrans()
                    onTransactionLoaded(uiState.transactions)
                    onBalanceExpenseIncomeLoaded(uiState.transactions)
                    Log.d(TAG, "observeTransaction: ${uiState.transactions.last().amount}")

                }

                is ViewState.Error -> {
                    binding.root.snack(
                        string = R.string.text_error
                    )

                }

                is ViewState.Empty -> {
                    hideRvTrans()
                }

            }


        }
    }

    private fun showRvTrans() = with(binding) {
        emptyStateLayout.hide()
        rvTransaction.show()
    }

    private fun hideRvTrans() = with(binding) {
        rvTransaction.hide()
        emptyStateLayout.show()
    }

    private fun onTransactionLoaded(tranList: List<Transaction>) {
        Log.d(TAG, "onTransactionLoaded: ")
        adapterTranList.clear()
        if (tranList.size <= 5) {
            tranList.forEach {
                adapterTranList.add(it)
            }
        } else {
            for (i in 0..4) {
                adapterTranList.add(tranList[i])
            }
        }

        return transactionAdapter.differ.submitList(adapterTranList)
    }


    private fun onBalanceExpenseIncomeLoaded(tranList: List<Transaction>) = with(binding) {
        val (incomeTrans, expenseTrans) =
            tranList.partition { it.transactionType == TransactionType.INCOME }


        totalIncome = incomeTrans.sumOf {
            it.amount
        }

        totalExpense = expenseTrans.sumOf {
            it.amount
        }

        Log.d(TAG, "onTotalTransactionLoaded: incomeTrans=$totalIncome--expenseTrans=$totalExpense")

        incomeCardView.tvTotalIncome.text = "+ ".plus(vietNamDong(totalIncome))
        expenseCardView.tvTotalExpense.text = "- ".plus(vietNamDong(totalExpense))
        totalBalanceView.tvTotalBalance.text = vietNamDong(totalIncome - totalExpense)

        Log.d(TAG, "onTotalTransactionLoaded: after update income, expense, balance to UI")
    }

    @SuppressLint("SimpleDateFormat", "RestrictedApi")
    private fun initViews() {
        transactionAdapter.setOnItemClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToTransactionDetailsFragment(
                transaction = it
            )
            findNavController().safeNavigate(action)
        }

        with(binding) {
            dashboardGroup.setOnClickListener(this@HomeFragment)
            tvSeeAllTransaction.setOnClickListener(this@HomeFragment)
            tvUserName.setOnClickListener(this@HomeFragment)
            ivAvatar.setOnClickListener(this@HomeFragment)
            btnConvert.setOnClickListener(this@HomeFragment)
            incomeCardView.totalIcon.setOnClickListener(this@HomeFragment)
            expenseCardView.totalIcon.setOnClickListener(this@HomeFragment)

        }

        binding.totalBalanceView.tvToday.text =
            SimpleDateFormat("dd - MM - yyyy").format(Calendar.getInstance().time)

    }


    private fun setupRV() {
        with(binding) {
            transactionAdapter = TransactionAdapter()
            rvTransaction.apply {
                adapter = transactionAdapter
                layoutManager = LinearLayoutManager(activity)
            }
        }
    }


    companion object {
        private const val TAG = "THAO HomeFragment"
    }

    @SuppressLint("RestrictedApi")
    override fun onClick(view: View?) {
        when (view) {
            binding.tvSeeAllTransaction -> {
                findNavController().safeNavigate(
                    HomeFragmentDirections.actionHomeFragmentToAllTransactionFragment()

                )
            }

            binding.tvUserName -> {
                findNavController().safeNavigate(
                    HomeFragmentDirections.actionHomeFragmentToProfileFragment()
                )

            }

            binding.ivAvatar -> {
                findNavController().safeNavigate(
                    HomeFragmentDirections.actionHomeFragmentToProfileFragment()
                )
            }

            binding.btnConvert -> {
                mainViewModel.convert(
                    binding.etFrom.text.toString(),
                    binding.spFromCurrency.selectedItem.toString(),
                    binding.spToCurrency.selectedItem.toString()
                )
            }

            binding.dashboardGroup -> {
                ViewUtils.hideKeyboard(view)
            }

            binding.incomeCardView.totalIcon -> {

                findNavController().navigate(
                    HomeFragmentDirections.actionHomeFragmentToAllTransactionFragment()
                )
            }

            binding.expenseCardView.totalIcon -> {

                findNavController().navigate(
                    HomeFragmentDirections.actionHomeFragmentToAllTransactionFragment()
                )

            }
        }

    }

    private var backPressedTime: Long = 0
    lateinit var backToast: Toast


    private fun pressAgainToExit() {
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    backToast = Toast.makeText(
                        requireContext(),
                        "Press back again to leave the app.",
                        Toast.LENGTH_LONG
                    )
                    if (backPressedTime + 2000 > System.currentTimeMillis()) {
                        backToast.cancel()
                        requireActivity().finish()
                        return
                    } else {
                        backToast.show()
                    }
                    backPressedTime = System.currentTimeMillis()
                }
            })
    }

}