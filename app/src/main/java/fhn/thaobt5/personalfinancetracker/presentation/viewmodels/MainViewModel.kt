package fhn.thaobt5.personalfinancetracker.presentation.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fhn.thaobt5.personalfinancetracker.data.models.api_model.Rates
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.models.User
import fhn.thaobt5.personalfinancetracker.domain.usecases.currency_api_usecase.GetRatesFromCurrencyApiUseCase
import fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.firebase_usecase.GetAllTransactionsByCurrentUserFromFireBase
import fhn.thaobt5.personalfinancetracker.domain.usecases.user_usecase.*
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import fhn.thaobt5.personalfinancetracker.utils.DispatcherProvider
import fhn.thaobt5.personalfinancetracker.utils.NetworkConfig
import fhn.thaobt5.personalfinancetracker.utils.api_state.ApiResource
import fhn.thaobt5.personalfinancetracker.utils.api_state.CurrencyApiEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import kotlin.math.round


@HiltViewModel
class MainViewModel @Inject constructor(
    private val loginUserUseCase: LoginUserUseCase,
    private val logoutUseCase: LogoutUseCase,
    private val signUpUserUseCase: SignUpUserUseCase,
    private val getCurrentUserUseCase: GetCurrentUserUseCase,
    private val getAllTransactionsByCurrentUserFromFireBase: GetAllTransactionsByCurrentUserFromFireBase,
    private val getRatesFromCurrencyApiUseCase: GetRatesFromCurrencyApiUseCase,
    private val dispatcher: DispatcherProvider

): ViewModel() {

    private val _conversion = MutableStateFlow<CurrencyApiEvent>(CurrencyApiEvent.Empty)
    val conversion: StateFlow<CurrencyApiEvent> = _conversion



    private val _signUpState = MutableStateFlow<FireBaseState<String>>(FireBaseState.Loading(""))
    val signUpState: StateFlow<FireBaseState<String>> get() = _signUpState


    private val _loginState = MutableStateFlow<FireBaseState<String>>(FireBaseState.Loading(""))
    val loginState: StateFlow<FireBaseState<String>> get() = _loginState



    private val _userData = MutableLiveData<User>()
    val userData: MutableLiveData<User> get() = _userData


    private val _transactionListFromFireBase = MutableLiveData<List<Transaction>>()
    val transactionListFromFireBase: LiveData<List<Transaction>> get() = _transactionListFromFireBase


    fun login(email: String, password: String, savePassChecked: Boolean) {
        viewModelScope.launch(dispatcher.io) {
            _loginState.update {
                loginUserUseCase(email, password, savePassChecked)
            }
        }
    }

    fun logout() {
        viewModelScope.launch(dispatcher.io) {
            _loginState.emit(FireBaseState.Fail(""))
            logoutUseCase()
        }
    }

    fun signUp(userName: String, email: String, password: String, repeatedPassword: String) {
        viewModelScope.launch {
            withContext(dispatcher.io) {
                _signUpState.update {
                    signUpUserUseCase(userName, email, password, repeatedPassword)
                }
            }
        }
    }



    fun getProfile() {
        CoroutineScope(dispatcher.io).launch {
            getCurrentUserUseCase().collect {
                _userData.postValue(it)
            }

        }
    }

    fun getTransactionListFromFirebase() {
        viewModelScope.launch(dispatcher.io) {
            getAllTransactionsByCurrentUserFromFireBase().collect {
                _transactionListFromFireBase.postValue(it)
            }

        }

    }

    fun convert(
        amountStr: String,
        fromCurrency: String,
        toCurrency: String
    ) {
        val fromAmount = amountStr.toFloatOrNull()
        if (fromAmount == null) {
            _conversion.value = CurrencyApiEvent.Failure("Not a valid amount")
            return
        }

        viewModelScope.launch(dispatcher.io) {
            _conversion.value = CurrencyApiEvent.Loading
            when (val ratesReponse = getRatesFromCurrencyApiUseCase(fromCurrency)) {
                is ApiResource.Error -> _conversion.value = CurrencyApiEvent.Failure(ratesReponse.message!!)
                is ApiResource.Success -> {
                    val rates = ratesReponse.data!!.rates
                    val rate = getRateForCurrency(toCurrency, rates)
                    if (rate == null) {
                        _conversion.value = CurrencyApiEvent.Failure("Unexpected error")
                    } else {

                        val convertedCurrency = round(fromAmount * rate * 100) / 100
                        _conversion.value = CurrencyApiEvent.Success(
                            "$fromAmount $fromCurrency = $convertedCurrency $toCurrency"
                        )
                    }

                }
            }
        }
    }

    private fun getRateForCurrency(currency: String, rates: Rates) = when (currency) {
        "CAD" -> rates.CAD
        "HKD" -> rates.HKD
        "ISK" -> rates.ISK
        "EUR" -> rates.EUR
        "PHP" -> rates.PHP
        "DKK" -> rates.DKK
        "HUF" -> rates.HUF
        "CZK" -> rates.CZK
        "AUD" -> rates.AUD
        "RON" -> rates.RON
        "SEK" -> rates.SEK
        "IDR" -> rates.IDR
        "INR" -> rates.INR
        "BRL" -> rates.BRL
        "JPY" -> rates.JPY
        "THB" -> rates.THB
        "CHF" -> rates.CHF
        "SGD" -> rates.SGD
        "PLN" -> rates.PLN
        "BGN" -> rates.BGN
        "CNY" -> rates.CNY
        "NOK" -> rates.NOK
        "NZD" -> rates.NZD
        "ZAR" -> rates.ZAR
        "USD" -> rates.USD
        "MXN" -> rates.MXN
        "ILS" -> rates.ILS
        "GBP" -> rates.GBP
        "KRW" -> rates.KRW
        "MYR" -> rates.MYR
        else -> null
    }



}