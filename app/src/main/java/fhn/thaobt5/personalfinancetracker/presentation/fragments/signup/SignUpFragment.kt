package fhn.thaobt5.personalfinancetracker.presentation.fragments.signup

import android.util.Log
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import fhn.thaobt5.personalfinancetracker.R
import fhn.thaobt5.personalfinancetracker.databinding.FragmentSignUpBinding
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import fhn.thaobt5.personalfinancetracker.presentation.base.BaseFragment
import fhn.thaobt5.personalfinancetracker.presentation.viewmodels.MainViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SignUpFragment : BaseFragment<FragmentSignUpBinding>(FragmentSignUpBinding::inflate) {

    private val viewModel: MainViewModel by activityViewModels()


    override fun observeViewModel() {
        super.observeViewModel()
        lifecycleScope.launch {
            viewModel.signUpState.collectLatest {
                doRegister(it)
            }
        }
    }

    override fun bindView() {
        super.bindView()
        binding.apply {
            tvLogin.setOnClickListener {
                if (findNavController().currentDestination?.id == R.id.signUpFragment) {
                    findNavController().navigate(
                        SignUpFragmentDirections.actionSignUpFragmentToLoginFragment()
                    )
                }
            }


            btnSignUp.setOnClickListener {
                viewModel.signUp(
                    userName = etUserName.text.toString(),
                    email = etEmail.text.toString(),
                    password = etPassword.text.toString(),
                    repeatedPassword = etConfirmPassword.text.toString()

                )
            }
        }
    }

    private fun doRegister(fireBaseState: FireBaseState<String>) {
        Log.d(TAG, "doRegister: start")
        when (fireBaseState) {
            is FireBaseState.Success -> {
                Toast.makeText(requireContext(), "Sign up success", Toast.LENGTH_LONG).show()
                if (findNavController().currentDestination?.id == R.id.signUpFragment) {
                    findNavController().navigate(
                        SignUpFragmentDirections.actionSignUpFragmentToLoginFragment()
                    )
                }
//                findNavController().popBackStack()
                Log.d(TAG, "doRegister: after success signup")
            }

            is FireBaseState.Fail -> {
                binding.tvError.isVisible = true
                binding.tvError.text = fireBaseState.message.toString()

//                with (binding) {
//                    if (etUserName.text?.isEmpty() == true) {
//                        etUserName.error = "User name must not be empty"
//                    }
//
//                    if (etEmail.text?.isEmpty() == true) {
//                        etEmail.error = "Email must not be empty"
//                    }
//
//                    if (etPassword.text?.isEmpty() == true) {
//                        etPassword.error = "Password must not be empty"
//                    }
//
//                    if (etConfirmPassword.text?.isEmpty()  == true) {
//                        etConfirmPassword.error = "Confirm-password must not be null"
//                    }
//
//                    else {
//                        tvError.isVisible = true
//                        tvError.text = fireBaseState.message.toString()
//                    }
//
//                }

            }

            is FireBaseState.Loading -> {

            }
        }
    }


    companion object {
       private const val TAG = "THAO SignUpFragment"
    }
}