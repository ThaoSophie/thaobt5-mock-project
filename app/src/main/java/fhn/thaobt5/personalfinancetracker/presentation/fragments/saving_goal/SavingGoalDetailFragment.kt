package fhn.thaobt5.personalfinancetracker.presentation.fragments.saving_goal

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import fhn.thaobt5.personalfinancetracker.databinding.FragmentAddTransactionBinding
import fhn.thaobt5.personalfinancetracker.databinding.FragmentSavingGoalDetailBinding
import fhn.thaobt5.personalfinancetracker.presentation.base.BaseFragment
import fhn.thaobt5.personalfinancetracker.presentation.fragments.transaction.details.TransactionDetailsFragmentArgs

@AndroidEntryPoint
class SavingGoalDetailFragment :
    BaseFragment<FragmentSavingGoalDetailBinding>(FragmentSavingGoalDetailBinding::inflate) {
    private val args by navArgs<SavingGoalDetailFragmentArgs>()

    override fun bindView() {
        super.bindView()
        updateSavingGoalContent()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    private fun updateSavingGoalContent() {
        (activity as AppCompatActivity).supportActionBar?.title = args.goal.title
        with(binding) {
            tvAmount.text = args.goal.amount.toString()
            tvStartDate.text = args.goal.startDate
            tvExpiryDate.text= args.goal.endDate

        }
    }
}