package fhn.thaobt5.personalfinancetracker.presentation.viewmodels


import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.TransactionUseCases
import fhn.thaobt5.personalfinancetracker.domain.usecases.user_usecase.UpdateUserInfoUseCase
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import fhn.thaobt5.personalfinancetracker.domain.utils.TransactionType
import fhn.thaobt5.personalfinancetracker.utils.DispatcherProvider
import fhn.thaobt5.personalfinancetracker.utils.viewState.DetailState
import fhn.thaobt5.personalfinancetracker.utils.viewState.ViewState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class TransactionViewModel @Inject constructor(
    private val transactionUseCases: TransactionUseCases,
    private val updateUserInfoUseCase: UpdateUserInfoUseCase,
    private val dispatcherProvider: DispatcherProvider

): ViewModel() {

    private val _addTransactionToFireBaseState = MutableLiveData<FireBaseState<String>>()
    val addTransactionToFireBaseState: LiveData<FireBaseState<String>> get() = _addTransactionToFireBaseState

    private val _updateTransactionToFireBaseState = MutableLiveData<FireBaseState<String>>()
    val updateTransactionToFireBaseState: LiveData<FireBaseState<String>> get() = _updateTransactionToFireBaseState

    private val _deleteTransactionFromFireBaseState = MutableLiveData<FireBaseState<String>>()
    val deleteTransactionFromFireBaseState: LiveData<FireBaseState<String>> get() = _deleteTransactionFromFireBaseState


    private val _transactionFilter = MutableStateFlow(TransactionType.OVERALL)
    val transactionFilter: StateFlow<TransactionType> = _transactionFilter

    private val _uiState = MutableStateFlow<ViewState>(ViewState.Loading)
    private val _detailState = MutableStateFlow<DetailState>(DetailState.Loading)

    // UI collect from this stateFlow to get the state updates
    val uiState: StateFlow<ViewState> = _uiState
    val detailState: StateFlow<DetailState> = _detailState

    private val _deleteTransactionRoomDBState = MutableStateFlow<ViewState>(ViewState.Loading)
    val deleteTrasactionRoomDBState: StateFlow<ViewState> = _deleteTransactionRoomDBState



    fun addTransactionToFireBase(transaction: Transaction) {
        viewModelScope.launch {
            withContext(dispatcherProvider.io) {
//                _addTransactionToFireBaseState.value =
//                    transactionUseCases.addTransactionToFireBase(transaction)
                _addTransactionToFireBaseState.postValue(
                    transactionUseCases.addTransactionToFireBase(transaction)
                )

            }
        }
    }

    fun updateTransactionToFireBase(transaction: Transaction) {
        viewModelScope.launch(dispatcherProvider.io) {
            _updateTransactionToFireBaseState.postValue(
                transactionUseCases.updateTransactionToFireBase(transaction)
            )
//            _updateTransactionToFireBaseState.value =
//                transactionUseCases.updateTransactionToFireBase(transaction)
        }
    }

    fun deleteTransactionFromFireBase(transaction: Transaction) {
        viewModelScope.launch(dispatcherProvider.io) {
            _deleteTransactionFromFireBaseState.postValue(
                transactionUseCases.deleteTransactionFromFireBase(transaction)
            )

        }
    }



    // insert transaction to TRANSACTION_DB in room db
    fun insertTransactionToRoomDB(transaction: Transaction) =
        viewModelScope.launch(dispatcherProvider.io) {
        transactionUseCases.insertTransactionToRoomDB(transaction)
    }

    // update user information(balance, expense, income) to User Firebase
    fun updateUserInforToFireBase(tranList: List<Transaction>) =
        viewModelScope.launch (dispatcherProvider.io) {
        val (totalIncome, totalExpense) =
            tranList.partition { it.transactionType == TransactionType.INCOME }

        val income = totalIncome.sumOf {
            it.amount.toLong()
        }

        val expense = totalExpense.sumOf {
            it.amount.toLong()
        }

        val balance = (income - expense)

        updateUserInfoUseCase(
            balance = balance,
            expense = expense,
            income = income
        )

    }

    // update transaction to TRANSACTION_DB in room db
    fun updateTransactionToRoomDB(transaction: Transaction) = viewModelScope.launch(dispatcherProvider.io) {
        transactionUseCases.updateTransactionToRoomDB(transaction)
    }

    // delete transaction from TRANSACTION_DB in ROOM
    fun deleteTransactionFromRoomDB(transaction: Transaction) = viewModelScope.launch(dispatcherProvider.io) {
        transactionUseCases.deleteTransactionFromRoomDB(transaction)
    }


    @SuppressLint("LongLogTag")
    fun getTransactionsByTypeFromRoomDB(type: TransactionType, userId: String) {
        viewModelScope.launch(dispatcherProvider.io) {
            transactionUseCases.getTransactionsByTypeFromRoomDB(type, userId).collect { result ->
                if (result.isNullOrEmpty()) {
                    _uiState.value = ViewState.Empty
                } else {
                    _uiState.value = ViewState.Success(result)
                    Log.d(TAG, "getTransactionsByType: ")
                }
            }
        }
    }

    @SuppressLint("LongLogTag")
    fun getAllTransactionFromRoomDB(userId: String)  = viewModelScope.launch(dispatcherProvider.io) {
        getTransactionsByTypeFromRoomDB(TransactionType.OVERALL, userId)
        Log.d(TAG, "getAllTransactionFromRoomDB: ")
    }

    fun getTranByTranIdFromRoomDB(tranId: Int) = viewModelScope.launch(dispatcherProvider.io) {
        _detailState.value = DetailState.Loading
        transactionUseCases.getTranByTranIdFromRoomDB(tranId).collect{ result: Transaction? ->
            if (result != null) {
                _detailState.value = DetailState.Success(result)
            }
        }
    }

    fun deleteByTranIdFromRoomDB(tranId: Int) = viewModelScope.launch(dispatcherProvider.io) {
        transactionUseCases.deleteTranByTranIdUseCase(tranId)
    }
    
    fun allIncome() {
        _transactionFilter.value = TransactionType.INCOME
    }

    fun allExpense() {
        _transactionFilter.value = TransactionType.EXPENSE
    }

    fun overall() {
        _transactionFilter.value = TransactionType.OVERALL
    }

    companion object {
        const val TAG = "Thao TransactionViewModel"
    }

}