package fhn.thaobt5.personalfinancetracker.presentation.fragments.login

import android.util.Log
import android.view.View
import android.view.View.OnClickListener
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import fhn.thaobt5.personalfinancetracker.data.local.SharedPreferenceHelper
import fhn.thaobt5.personalfinancetracker.databinding.FragmentLoginBinding
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import fhn.thaobt5.personalfinancetracker.presentation.base.BaseFragment
import fhn.thaobt5.personalfinancetracker.presentation.viewmodels.MainViewModel
import fhn.thaobt5.personalfinancetracker.presentation.viewmodels.TransactionViewModel
import fhn.thaobt5.personalfinancetracker.utils.safeNavigate
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject


@AndroidEntryPoint
class LoginFragment :
    BaseFragment<FragmentLoginBinding>(FragmentLoginBinding::inflate), OnClickListener {

    private val mainViewModel: MainViewModel by activityViewModels()
    private val transactionViewModel: TransactionViewModel by viewModels()

    override fun observeViewModel() {
        super.observeViewModel()
        lifecycleScope.launch {
            mainViewModel.loginState.collectLatest{
                doLogin(it)
            }
        }

    }



    override fun bindView() {
        super.bindView()

        with (binding) {
            btnLogin.setOnClickListener(this@LoginFragment)
            tvSignUp.setOnClickListener(this@LoginFragment)
            cbSavePassword.setOnClickListener(this@LoginFragment)
        }
    }

    override fun onClick(view: View?) {
        with(binding) {
            when (view) {
                tvSignUp -> {
                    findNavController().safeNavigate(
                        LoginFragmentDirections.actionLoginFragmentToSignUpFragment()
                    )

                }

                btnLogin -> {

                    mainViewModel.login(
                        etEmail.text.toString(),
                        etPassword.text.toString(),
                        cbSavePassword.isChecked
                    )
                }
            }
        }
    }


    private fun doLogin(loginState: FireBaseState<String>) {
        when (loginState) {
            is FireBaseState.Success -> {
                Log.d(TAG, "doLogin: login Success")
                Toast.makeText(requireContext(), "Login success", Toast.LENGTH_SHORT).show()
                findNavController().safeNavigate(
                    LoginFragmentDirections.actionLoginFragmentToHomeFragment2()
                )

            }

            is FireBaseState.Fail -> {
                Log.d(TAG, "doLogin: login Fail")
                binding.tvError.isVisible = true
                binding.tvError.text = loginState.message.toString()


//                with (binding) {
//                    if (etEmail.text?.isEmpty() == true) {
//                        etEmail.error = "Email must not be empty"
//                    }
//                    if (etPassword.text?.isEmpty() == true) {
//                        etPassword.error = "Password must not be empty"
//                    }
//
//                    else {
//                        tvError.isVisible = true
//                        tvError.text = loginState.message.toString()
//                    }
//                }

            }

            else -> {}
        }
    }

    companion object {
        private const val TAG = "THAO LoginFragment"
    }



}