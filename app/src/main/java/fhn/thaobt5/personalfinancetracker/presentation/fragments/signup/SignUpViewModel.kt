package fhn.thaobt5.personalfinancetracker.presentation.fragments.signup

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fhn.thaobt5.personalfinancetracker.domain.usecases.user_usecase.SignUpUserUseCase
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel @Inject constructor(
    private val signUpUserUseCase: SignUpUserUseCase
) : ViewModel() {

    private val _signUpState = MutableStateFlow<FireBaseState<String>>(FireBaseState.Loading(""))
    val signUpState: StateFlow<FireBaseState<String>> get() = _signUpState

    fun signUp(userName: String, email: String, password: String, repeatedPassword: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _signUpState.value = signUpUserUseCase(userName, email, password, repeatedPassword)

            }
        }
    }
}