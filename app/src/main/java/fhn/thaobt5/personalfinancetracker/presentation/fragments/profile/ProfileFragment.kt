package fhn.thaobt5.personalfinancetracker.presentation.fragments.profile

import android.view.View
import android.view.View.OnClickListener
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import fhn.thaobt5.personalfinancetracker.databinding.FragmentProfileBinding
import fhn.thaobt5.personalfinancetracker.domain.models.User
import fhn.thaobt5.personalfinancetracker.presentation.base.BaseFragment
import fhn.thaobt5.personalfinancetracker.presentation.viewmodels.MainViewModel
import fhn.thaobt5.personalfinancetracker.utils.safeNavigate
import fhn.thaobt5.personalfinancetracker.utils.vietNamDong
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ProfileFragment :
    BaseFragment<FragmentProfileBinding>(FragmentProfileBinding::inflate), OnClickListener {

    private val mainViewModel: MainViewModel by activityViewModels()

    override fun observeViewModel() {
        super.observeViewModel()
        lifecycleScope.launch {
            mainViewModel.userData.observe(viewLifecycleOwner) {
                updateUserInfo(it)
            }
        }
    }

    override fun bindView() {
        super.bindView()
        with(binding) {
            btnProfileSignout.setOnClickListener(this@ProfileFragment)
        }
    }

    private fun updateUserInfo(user: User) {
        with(binding) {
            tvProfileName.text = user.userName
            tvProfileBalance.text = vietNamDong(user.balance.toDouble())
        }
    }

    companion object {
        const val TAG = "Thao ProfileFragment"
    }

    override fun onClick(view: View?) {
        when (view) {

            binding.btnProfileSignout -> {
                mainViewModel.logout()
                findNavController().safeNavigate(
                    ProfileFragmentDirections.actionProfileFragmentToLoginFragment()
                )
            }
        }
    }


}