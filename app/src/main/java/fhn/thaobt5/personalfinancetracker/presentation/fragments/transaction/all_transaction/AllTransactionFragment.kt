package fhn.thaobt5.personalfinancetracker.presentation.fragments.transaction.all_transaction

import android.annotation.SuppressLint
import android.graphics.Color
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.View.OnClickListener
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import fhn.thaobt5.personalfinancetracker.R
import fhn.thaobt5.personalfinancetracker.databinding.FragmentAllTransactionBinding
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.utils.TransactionType
import fhn.thaobt5.personalfinancetracker.presentation.adapter.TransactionAdapter
import fhn.thaobt5.personalfinancetracker.presentation.base.BaseFragment
import fhn.thaobt5.personalfinancetracker.presentation.viewmodels.MainViewModel
import fhn.thaobt5.personalfinancetracker.presentation.viewmodels.TransactionViewModel
import fhn.thaobt5.personalfinancetracker.utils.*
import fhn.thaobt5.personalfinancetracker.utils.viewState.ViewState
import kotlinx.coroutines.launch

@AndroidEntryPoint
class AllTransactionFragment :
    BaseFragment<FragmentAllTransactionBinding>(FragmentAllTransactionBinding::inflate)
{

    private val mainViewModel: MainViewModel by activityViewModels()
    private val transactionViewModel: TransactionViewModel by viewModels()

    private lateinit var transactionAdapter: TransactionAdapter
    private var currentUserID: String? = null

    override fun observeViewModel() {
        super.observeViewModel()

        transactionViewModel.overall()

        mainViewModel.userData.observe(viewLifecycleOwner) {
            observeFilter(it.userId)
            currentUserID = it.userId
        }

        setupRV()
        initViews()
        swipeToDelete()
        observeTransaction()

        updateUserInfoDataToFireBase()
    }

    private fun updateUserInfoDataToFireBase() = lifecycleScope.launch {
        transactionViewModel.uiState.collect {uiState ->
            when (uiState) {
                is ViewState.Success -> {
                    transactionViewModel.updateUserInforToFireBase(uiState.transactions)

                    Log.d("Thao HomeFragment", "updateUserInfoDataToFireBase: , line=77")
                }
                else -> {}
            }

        }

    }

    private fun swipeToDelete() {
        // init item touch callback for swipe action
        val itemTouchHelperCallback = object : ItemTouchHelper.SimpleCallback(
            ItemTouchHelper.UP or ItemTouchHelper.DOWN,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return true
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                // get item position & delete notes
                val position = viewHolder.adapterPosition
                val transactionItem = transactionAdapter.differ.currentList[position]

                transactionViewModel.deleteTransactionFromRoomDB(transactionItem)
                transactionViewModel.deleteTransactionFromFireBase(transactionItem)
                transactionViewModel.getAllTransactionFromRoomDB(currentUserID!!)
                transactionAdapter.notifyDataSetChanged()
                Snackbar.make(
                    binding.root,
                    getString(R.string.success_transaction_delete),
                    Snackbar.LENGTH_SHORT
                ).apply {
//                    setAction(getString(R.string.text_undo)) {
//                        transactionViewModel.insertTransactionToRoomDB(
//                            transactionItem
//                        )
//                        transactionViewModel.addTransactionToFireBase(transactionItem)
//                        transactionViewModel.getAllTransactionFromRoomDB(currentUserID!!)
//                        transactionAdapter.notifyDataSetChanged()
//                    }
                    show()
                }
            }
        }

        ItemTouchHelper(itemTouchHelperCallback).apply {
            attachToRecyclerView(binding.rvAllTransactions)
        }
    }



    @SuppressLint("LongLogTag")
    private fun observeFilter(userId: String) = with(binding) {
        Log.d(TAG, "observeFilter: userId=$userId")
        lifecycleScope.launch {
            transactionViewModel.transactionFilter.collect {transactionType ->
                when (transactionType) {
                    TransactionType.OVERALL -> {
                        tvAllTransactions.text = getString(R.string.all_transactions)


                    }

                    TransactionType.INCOME -> {
                        tvAllTransactions.text = getString(R.string.all_incomes)
                    }

                    TransactionType.EXPENSE -> {
                        tvAllTransactions.text = getString(R.string.all_expense)
                    }
                }
                transactionViewModel.getTransactionsByTypeFromRoomDB(transactionType, userId)

            }

        }

    }

    @SuppressLint("LongLogTag")
    private fun observeTransaction() = lifecycleScope.launch {
        transactionViewModel.uiState.collect {uiState ->
            when (uiState) {
                is ViewState.Success -> {
                    showRvTrans()
                    onTransactionLoaded(uiState.transactions)
                    Log.d(TAG, "observeTransaction: ${uiState.transactions.last().amount}")

                }

                is ViewState.Error -> {
                    binding.root.snack(
                        string = R.string.text_error
                    )

                }

                is ViewState.Empty -> {
                    hideRvTrans()
                }

                is ViewState.Loading -> {

                }

            }


        }
    }

    private fun showRvTrans() = with(binding) {
        emptyStateLayout.hide()
        rvAllTransactions.show()
    }

    private fun hideRvTrans() = with(binding) {
        rvAllTransactions.hide()
        emptyStateLayout.show()
    }

    private fun onTransactionLoaded(tranList: List<Transaction>) =
        transactionAdapter.differ.submitList(tranList)


    private fun setupRV() {
        with(binding) {
            transactionAdapter = TransactionAdapter()
            rvAllTransactions.apply {
                adapter = transactionAdapter
                layoutManager = LinearLayoutManager(activity)
            }
        }
    }

    private fun initViews() {
        transactionAdapter.setOnItemClickListener {
            val action =
                AllTransactionFragmentDirections.actionAllTransactionFragmentToTransactionDetailsFragment(
                    transaction = it
                )
            findNavController().safeNavigate(action)
        }

        setClickTransactionFilterSpinner()
    }

    private fun setClickTransactionFilterSpinner() {
        val adapter = ArrayAdapter.createFromResource(
            applicationContext(),
            R.array.allFilters,
            android.R.layout.simple_spinner_item
        )

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerTransactionFilter.adapter = adapter

        binding.spinnerTransactionFilter.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                lifecycleScope.launch {
                    when (position) {
                        0 -> {
                            transactionViewModel.overall()
                        }

                        1 -> {
                            transactionViewModel.allIncome()
                        }

                        2 -> {
                            transactionViewModel.allExpense()

                        }
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                lifecycleScope.launch {
                    transactionViewModel.overall()
                }
            }

        }
    }



    override fun bindView() {
        super.bindView()
    }

    companion object {
        private const val TAG = "THAO AllTransactionFragment"
    }

}
