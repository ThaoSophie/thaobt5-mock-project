package fhn.thaobt5.personalfinancetracker.presentation.fragments.saving_goal

import android.util.Log
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import fhn.thaobt5.personalfinancetracker.databinding.FragmentSavingGoalsBinding
import fhn.thaobt5.personalfinancetracker.domain.models.SavingGoal
import fhn.thaobt5.personalfinancetracker.presentation.adapter.SavingGoalAdapter
import fhn.thaobt5.personalfinancetracker.presentation.base.BaseFragment
import fhn.thaobt5.personalfinancetracker.presentation.fragments.home.HomeFragmentDirections
import fhn.thaobt5.personalfinancetracker.presentation.viewmodels.MainViewModel
import fhn.thaobt5.personalfinancetracker.utils.safeNavigate
import fhn.thaobt5.personalfinancetracker.utils.show
import fhn.thaobt5.personalfinancetracker.utils.vietNamDong
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class SavingGoalsFragment :
    BaseFragment<FragmentSavingGoalsBinding>(FragmentSavingGoalsBinding::inflate)

{

    private val mainViewModel: MainViewModel by activityViewModels()
    private val savingGoalsViewModel: SavingGoalsViewModel by viewModels()
    private lateinit var savingGoalAdapter: SavingGoalAdapter
    private var totalGoal = 0.0
    private var currentUserID: String? = null
    private var savingGoalList = listOf<SavingGoal>()

    override fun observeViewModel() {
        super.observeViewModel()

        mainViewModel.getProfile()
        savingGoalsViewModel.getAllSavingGoalFireBase()


        lifecycleScope.launch {
            mainViewModel.userData.observe(viewLifecycleOwner) {
                currentUserID = it.userId
            }

            savingGoalsViewModel.savingGoalList.observe(viewLifecycleOwner) {
                updateUI(it)
            }
        }

        initViews()



    }

    private fun initViews() {
        binding.btnAddSavingGoal.setOnClickListener {
            Log.d("Thao", "initViews: , line=58: press add saving goal button")
            findNavController().navigate(
                SavingGoalsFragmentDirections.actionSavingGoalsFragmentToAddSavingGoalFragment()
            )
        }
    }

    private fun updateUI(savingGoalList: List<SavingGoal>) {
        if (savingGoalList.size == 0) {
            binding.emptyStateGoalLayout.show()
            binding.emptyStateGoalLayout
        }

        savingGoalAdapter = SavingGoalAdapter()
        savingGoalAdapter.submitList(savingGoalList)
        savingGoalAdapter.setOnItemClickListener {
            findNavController().navigate(
                SavingGoalsFragmentDirections.actionSavingGoalsFragmentToSavingGoalDetailFragment(
                    it
                )
            )

        }
        binding.rvSavingGoals.apply {
            adapter = savingGoalAdapter
            layoutManager = LinearLayoutManager(activity)
        }

        loadTotalGoalAmount(savingGoalList)
        loadTotalBooks(savingGoalList)

    }

    private fun loadTotalGoalAmount(savingGoalList: List<SavingGoal>) {
        var sum = 0.0
        savingGoalList.forEach {
            sum += it.amount!!
        }

        binding.tvTotal.text = vietNamDong(sum)

    }

    private fun loadTotalBooks(savingGoalList: List<SavingGoal>) {
        binding.tvTotalBooks.text = savingGoalList.size.toString()
    }

    override fun bindView() {
        super.bindView()
        savingGoalsViewModel.getAllSavingGoalFireBase()
    }





}