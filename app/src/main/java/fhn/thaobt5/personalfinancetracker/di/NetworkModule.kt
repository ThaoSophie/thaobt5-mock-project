package fhn.thaobt5.personalfinancetracker.di

import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import fhn.thaobt5.personalfinancetracker.data.network.api.CurrencyApi
import fhn.thaobt5.personalfinancetracker.data.network.firebase.*
import fhn.thaobt5.personalfinancetracker.data.repository.CurrencyApiRepositoryImpl
import fhn.thaobt5.personalfinancetracker.data.utils.NetworkControl
import fhn.thaobt5.personalfinancetracker.domain.repository.CurrencyApiRepository
import fhn.thaobt5.personalfinancetracker.utils.DispatcherProvider
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

private const val BASE_URL = "https://anyapi.io/"


@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideCurrencyApi(): CurrencyApi = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(CurrencyApi::class.java)

    @Provides
    @Singleton
    fun provideCurrencyApiRepository(api: CurrencyApi): CurrencyApiRepository = CurrencyApiRepositoryImpl(api)

    @Singleton
    @Provides
    fun provideDispatchers(): DispatcherProvider = object : DispatcherProvider {
        override val main: CoroutineDispatcher
            get() = Dispatchers.Main

        override val io: CoroutineDispatcher
            get() = Dispatchers.IO

        override val defaultL: CoroutineDispatcher
            get() = Dispatchers.Default

        override val unconfined: CoroutineDispatcher
            get() = Dispatchers.Unconfined

    }



    @Provides
    @Singleton
    fun provideFirebaseAuth(): FirebaseAuth {
        return Firebase.auth
    }


    @Provides
    @Singleton
    fun provideUserDataSource(
        firebaseAuth: FirebaseAuth,
        databaseReference: DatabaseReference
    ): UserDataSource {
        return UserDataSourceImpl(firebaseAuth, databaseReference)
    }


    @Provides
    @Singleton
    fun provideTransactionDataSource(
        firebaseAuth: FirebaseAuth,
        databaseReference: DatabaseReference
    ): TransactionDataSource {
        return TransactionDataSourceImpl(firebaseAuth, databaseReference)

    }

    @Provides
    @Singleton
    fun provideSavingGoalDataSource(
        firebaseAuth: FirebaseAuth,
        databaseReference: DatabaseReference
    ): SavingGoalDataSource {
        return SavingGoalDataSourceImpl(firebaseAuth, databaseReference)
    }

    @Provides
    @Singleton
    fun provideDatabaseReference(): DatabaseReference {
        return Firebase.database.reference
    }

    @Provides
    @Singleton
    fun provideNetWorkControl(@ApplicationContext context: Context): NetworkControl {
        return NetworkControl(context)

    }


}