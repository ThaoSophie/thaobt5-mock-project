package fhn.thaobt5.personalfinancetracker.di

import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import fhn.thaobt5.personalfinancetracker.data.local.AppDatabase
import fhn.thaobt5.personalfinancetracker.domain.repository.TransactionRepository
import fhn.thaobt5.personalfinancetracker.domain.repository.UserRepository
import fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.firebase_usecase.AddTransactionToFireBase
import fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.TransactionUseCases
import fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.firebase_usecase.DeleteTransactionFromFireBase
import fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.firebase_usecase.GetAllTransactionsByCurrentUserFromFireBase
import fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.firebase_usecase.UpdateTransactionToFireBase
import fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.roomdb_usecase.*
import fhn.thaobt5.personalfinancetracker.utils.MY_SHARED_PREFERENCE
import fhn.thaobt5.personalfinancetracker.utils.TRANSACTION_DB
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object LocalModule {

    @Provides
    @Singleton
    fun provideSharedPreference(@ApplicationContext context: Context): SharedPreferences {
        return context.getSharedPreferences(MY_SHARED_PREFERENCE, Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, TRANSACTION_DB)
            .fallbackToDestructiveMigration().build()
    }

    @Provides
    @Singleton
    fun provideTransactionUseCase(
        userRepo: UserRepository,
        tranRepo: TransactionRepository
    ): TransactionUseCases {
        return TransactionUseCases(
            deleteTranByTranIdUseCase = DeleteTranByTranIdUseCase(tranRepo),
            deleteTransactionFromRoomDB = DeleteTransactionFromRoomDB(tranRepo),
            getTranByTranIdFromRoomDB = GetTranByTranIdFromRoomDB(tranRepo),
            getTransactionsByTypeFromRoomDB = GetTransactionsByTypeFromRoomDB(tranRepo),
            getTransactionsByUserIDFromRoomDB = GetTransactionsByUserIDFromRoomDB(tranRepo),
            insertTransactionToRoomDB = InsertTransactionToRoomDB(tranRepo),
            updateTransactionToRoomDB = UpdateTransactionToRoomDB(tranRepo),
            getAllTransactionsByCurrentUserFromFireBase = GetAllTransactionsByCurrentUserFromFireBase(userRepo),
            addTransactionToFireBase = AddTransactionToFireBase(userRepo),
            updateTransactionToFireBase = UpdateTransactionToFireBase(userRepo),
            deleteTransactionFromFireBase = DeleteTransactionFromFireBase(userRepo)
        )
    }
}