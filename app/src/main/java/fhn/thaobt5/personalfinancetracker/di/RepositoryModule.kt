package fhn.thaobt5.personalfinancetracker.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import fhn.thaobt5.personalfinancetracker.data.local.AppDatabase
import fhn.thaobt5.personalfinancetracker.data.network.firebase.SavingGoalDataSource
import fhn.thaobt5.personalfinancetracker.data.network.firebase.TransactionDataSource
import fhn.thaobt5.personalfinancetracker.data.network.firebase.UserDataSource
import fhn.thaobt5.personalfinancetracker.data.repository.TransactionRepositoryImpl
import fhn.thaobt5.personalfinancetracker.data.repository.UserRepositoryFirebaseImpl
import fhn.thaobt5.personalfinancetracker.domain.repository.TransactionRepository
import fhn.thaobt5.personalfinancetracker.domain.repository.UserRepository
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun provideUserRepository(
        userDataSource: UserDataSource,
        transactionDataSource: TransactionDataSource,
        savingGoalDataSource: SavingGoalDataSource
    ): UserRepository {
        return UserRepositoryFirebaseImpl(
            userDataSource,
            transactionDataSource,
            savingGoalDataSource
        )
    }

    @Provides
    @Singleton
    fun provideTranRepository(database: AppDatabase): TransactionRepository {
        return TransactionRepositoryImpl(database)
    }
}