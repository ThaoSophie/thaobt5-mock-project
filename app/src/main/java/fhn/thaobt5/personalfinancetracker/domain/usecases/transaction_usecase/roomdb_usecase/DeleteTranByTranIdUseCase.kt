package fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.roomdb_usecase

import fhn.thaobt5.personalfinancetracker.domain.repository.TransactionRepository
import javax.inject.Inject

class DeleteTranByTranIdUseCase @Inject constructor(
    private val transactionRepo: TransactionRepository
) {

    suspend operator fun invoke(tranId: Int) = transactionRepo.deleteTransactionByID(tranId)
}