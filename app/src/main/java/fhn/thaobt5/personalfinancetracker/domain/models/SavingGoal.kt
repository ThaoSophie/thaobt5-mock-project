package fhn.thaobt5.personalfinancetracker.domain.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class SavingGoal (
    var title: String? = null,

    var amount: Double? = null,

    var startDate: String? = null,

    var endDate: String? = null
): Parcelable