package fhn.thaobt5.personalfinancetracker.domain.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.firebase.database.IgnoreExtraProperties
import fhn.thaobt5.personalfinancetracker.data.utils.TRANSACTION_TABLE
import fhn.thaobt5.personalfinancetracker.domain.utils.TransactionTag
import fhn.thaobt5.personalfinancetracker.domain.utils.TransactionType
import kotlinx.parcelize.Parcelize
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Calendar

@IgnoreExtraProperties
@Parcelize
@Entity(tableName = TRANSACTION_TABLE)
data class Transaction(

    var userId: String = "",

    var title: String = "",

    var amount: Double = 0.0,

    var transactionType: TransactionType? = TransactionType.OVERALL,

    var tag: TransactionTag?,

    var date: String = SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().time),

    var note: String = "",

    @PrimaryKey(autoGenerate = true)
    var tranId: Int = 0,

    var createdAt: Long = System.currentTimeMillis(),

): Parcelable {

    val createdAtDateFormat: String
        get() = DateFormat.getDateTimeInstance()
            .format(createdAt)  // Date Format: Jan 11, 2021, 11:30 AM
}
