package fhn.thaobt5.personalfinancetracker.domain.usecases.saving_usecases

import fhn.thaobt5.personalfinancetracker.domain.models.SavingGoal
import fhn.thaobt5.personalfinancetracker.domain.repository.UserRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetAllSavingGoalFireBaseUseCase @Inject constructor(
    private val userRepository: UserRepository
) {

    operator fun invoke(): Flow<List<SavingGoal>> {
        return userRepository.getSavingGoalByCurrentUserFireBase()
    }
}