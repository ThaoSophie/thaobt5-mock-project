package fhn.thaobt5.personalfinancetracker.domain.usecases.user_usecase

data class UserUseCases(
    val getCurrentUserUseCase: GetCurrentUserUseCase,
    val loginUserUseCase: LoginUserUseCase,
    val signUpUserUseCase: SignUpUserUseCase,
    val updateUserInfoUseCase: UpdateUserInfoUseCase

)
