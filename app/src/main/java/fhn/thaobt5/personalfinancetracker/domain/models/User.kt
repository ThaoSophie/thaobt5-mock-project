package fhn.thaobt5.personalfinancetracker.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.firebase.database.IgnoreExtraProperties
import fhn.thaobt5.personalfinancetracker.data.utils.USER_TABLE


@IgnoreExtraProperties
@Entity(tableName = USER_TABLE)
data class User(
    @PrimaryKey
    val userId: String = "",

    var userName: String = "",

    var email: String = "",

    var password: String = "",

    var balance: Long = 0,

    var totalIncome: Long = 0,

    var totalExpense: Long = 0,

    )
