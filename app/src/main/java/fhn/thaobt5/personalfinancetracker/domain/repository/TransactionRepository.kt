package fhn.thaobt5.personalfinancetracker.domain.repository

import androidx.room.Query
import fhn.thaobt5.personalfinancetracker.data.utils.TRANSACTION_TABLE
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.utils.TransactionType
import kotlinx.coroutines.flow.Flow

interface TransactionRepository {

    suspend fun insert(transaction: Transaction)

    suspend fun update(transaction: Transaction)

    suspend fun delete(transaction: Transaction)

    // get all transaction
    fun getAllTransactions(): Flow<List<Transaction>>

    fun getTransactionByType(
        transactionType: TransactionType,
        userId: String
    ): Flow<List<Transaction>>


    // get all transaction by userId
    fun getTransactionsByUserID(userId: String): Flow<List<Transaction>>

    // get single transaction by tranId
    fun getTransactionByTranID(tranId: Int): Flow<Transaction>


    // delete transaction by id
    suspend fun deleteTransactionByID(tranId: Int)
}