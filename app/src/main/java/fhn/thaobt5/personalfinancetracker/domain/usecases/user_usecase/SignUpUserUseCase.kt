package fhn.thaobt5.personalfinancetracker.domain.usecases.user_usecase

import fhn.thaobt5.personalfinancetracker.data.local.SharedPreferenceHelper
import fhn.thaobt5.personalfinancetracker.domain.models.User
import fhn.thaobt5.personalfinancetracker.domain.repository.UserRepository
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import fhn.thaobt5.personalfinancetracker.utils.InputFieldValidator
import javax.inject.Inject

class SignUpUserUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val sharedPreferenceHelper: SharedPreferenceHelper
) {

    suspend operator fun invoke(
        userName: String, email: String, password: String, repeatedPassword: String
    ): FireBaseState<String> {
        return when {
            userName.isEmpty() -> FireBaseState.Fail("User name must not be empty")
            email.isEmpty() -> FireBaseState.Fail("Email must not be empty")
            password.isEmpty() -> FireBaseState.Fail("Password must not be empty")
            repeatedPassword.isEmpty() -> FireBaseState.Fail("Please enter confirm-password")
            password != repeatedPassword -> FireBaseState.Fail("Confirm-password must be the same as password")
            !InputFieldValidator.isValidEmail(email) -> FireBaseState.Fail("Email is invalid")
            !InputFieldValidator.isValidPassword(password) -> FireBaseState.Fail("Password is invalid")
            else -> {
                val user = User(userName = userName, email = email, password = password)
                val signUpResponseState = userRepository.signUp(user)

                if (signUpResponseState is FireBaseState.Success) {
                    sharedPreferenceHelper.setEmail(email)
                    sharedPreferenceHelper.setPassword(password)
                    sharedPreferenceHelper.setUserName(userName)
                }

                return signUpResponseState
            }

        }
    }
}