package fhn.thaobt5.personalfinancetracker.domain.utils

sealed class FireBaseState<T> (
    val data: T? = null,
    val message: String? = null
) {
    class Success<T>(data: T?): FireBaseState<T>(data, null)
    class Fail<T>(msg: String?): FireBaseState<T>(null, msg)
    class Loading<T>(msg: String): FireBaseState<T>(null, msg)
}

