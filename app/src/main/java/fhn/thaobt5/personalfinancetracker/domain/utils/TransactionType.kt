package fhn.thaobt5.personalfinancetracker.domain.utils

enum class TransactionType(

) {
    EXPENSE,
    INCOME,
    OVERALL
}

