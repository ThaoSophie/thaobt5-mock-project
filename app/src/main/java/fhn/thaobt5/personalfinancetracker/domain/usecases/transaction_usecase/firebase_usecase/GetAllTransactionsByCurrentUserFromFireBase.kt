package fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.firebase_usecase

import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.repository.UserRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetAllTransactionsByCurrentUserFromFireBase @Inject constructor(
    private val userRepository: UserRepository
) {

    suspend operator fun invoke(): Flow<List<Transaction>> = userRepository.getAllTransactionsByCurrentUser()
}