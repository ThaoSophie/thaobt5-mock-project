package fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.roomdb_usecase

import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.repository.TransactionRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetTransactionsByUserIDFromRoomDB @Inject constructor(
    private val transactionRepo: TransactionRepository
) {

    operator fun invoke(userId: String): Flow<List<Transaction>> {
        return transactionRepo.getTransactionsByUserID(userId)
    }
}