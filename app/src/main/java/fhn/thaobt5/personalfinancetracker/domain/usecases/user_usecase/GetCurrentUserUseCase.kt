package fhn.thaobt5.personalfinancetracker.domain.usecases.user_usecase

import fhn.thaobt5.personalfinancetracker.domain.models.User
import fhn.thaobt5.personalfinancetracker.domain.repository.UserRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetCurrentUserUseCase @Inject constructor(
    private val userRepository: UserRepository
) {

    operator fun invoke(): Flow<User> = userRepository.getCurrentUser()
}