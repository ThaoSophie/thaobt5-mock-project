package fhn.thaobt5.personalfinancetracker.domain.usecases.user_usecase

import fhn.thaobt5.personalfinancetracker.data.local.SharedPreferenceHelper
import fhn.thaobt5.personalfinancetracker.domain.repository.UserRepository
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import javax.inject.Inject

class LogoutUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val sharedPreferenceHelper: SharedPreferenceHelper
) {

    suspend operator fun invoke(): FireBaseState<String> {
        val logoutResponseState = userRepository.logout()
        if (logoutResponseState is FireBaseState.Success) {
            sharedPreferenceHelper.setEmail("")
            sharedPreferenceHelper.setPassword("")
            sharedPreferenceHelper.setIsRemember(false)
            sharedPreferenceHelper.setIsLoggedIn(false)
            sharedPreferenceHelper.setIsOpenFirstTime(false)
        }
        return logoutResponseState
    }
}