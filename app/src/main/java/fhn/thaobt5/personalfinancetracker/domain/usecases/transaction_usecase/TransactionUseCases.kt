package fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase

import fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.firebase_usecase.AddTransactionToFireBase
import fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.firebase_usecase.DeleteTransactionFromFireBase
import fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.firebase_usecase.GetAllTransactionsByCurrentUserFromFireBase
import fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.firebase_usecase.UpdateTransactionToFireBase
import fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.roomdb_usecase.*

data class TransactionUseCases(
    val deleteTranByTranIdUseCase: DeleteTranByTranIdUseCase,
    val deleteTransactionFromRoomDB: DeleteTransactionFromRoomDB,
    val getTranByTranIdFromRoomDB: GetTranByTranIdFromRoomDB,
    val getTransactionsByTypeFromRoomDB: GetTransactionsByTypeFromRoomDB,
    val getTransactionsByUserIDFromRoomDB: GetTransactionsByUserIDFromRoomDB,
    val insertTransactionToRoomDB: InsertTransactionToRoomDB,
    val updateTransactionToRoomDB: UpdateTransactionToRoomDB,
    val getAllTransactionsByCurrentUserFromFireBase: GetAllTransactionsByCurrentUserFromFireBase,
    val addTransactionToFireBase: AddTransactionToFireBase,
    val updateTransactionToFireBase: UpdateTransactionToFireBase,
    val deleteTransactionFromFireBase: DeleteTransactionFromFireBase

)
