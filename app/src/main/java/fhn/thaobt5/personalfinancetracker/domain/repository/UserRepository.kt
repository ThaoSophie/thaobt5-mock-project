package fhn.thaobt5.personalfinancetracker.domain.repository

import fhn.thaobt5.personalfinancetracker.domain.models.SavingGoal
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.models.User
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import kotlinx.coroutines.flow.Flow

interface UserRepository {
    suspend fun login(email: String, password: String): FireBaseState<String>
    suspend fun signUp(user: User): FireBaseState<String>

    fun logout(): FireBaseState<String>



    suspend fun updateUserBalance(balance: Long)
    suspend fun updateUserExpense(expense: Long)

    suspend fun updateUserIncome(income: Long)


    // update transactions for user
    suspend fun addATransaction(transactions: Transaction): FireBaseState<String>

    suspend fun updateTransaction(transaction: Transaction): FireBaseState<String>

    suspend fun deleteTransaction(transaction: Transaction): FireBaseState<String>

    suspend fun getAllTransactionsByCurrentUser(): Flow<List<Transaction>>

    fun getSavingGoalByCurrentUserFireBase(): Flow<List<SavingGoal>>

    suspend fun addSavingGoalFireBase(savingGoal: SavingGoal): FireBaseState<String>

    // get current user
    fun getCurrentUser(): Flow<User>

}