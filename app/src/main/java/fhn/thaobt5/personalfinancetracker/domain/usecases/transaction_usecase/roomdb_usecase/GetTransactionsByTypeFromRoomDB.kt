package fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.roomdb_usecase

import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.repository.TransactionRepository
import fhn.thaobt5.personalfinancetracker.domain.utils.TransactionType
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetTransactionsByTypeFromRoomDB @Inject constructor(
    private val transactionRepo: TransactionRepository
) {

    operator fun invoke(type: TransactionType, userId: String): Flow<List<Transaction>> {
        return transactionRepo.getTransactionByType(type, userId)
    }
}