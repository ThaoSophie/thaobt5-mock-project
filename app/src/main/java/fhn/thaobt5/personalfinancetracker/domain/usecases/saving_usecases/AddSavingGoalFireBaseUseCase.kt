package fhn.thaobt5.personalfinancetracker.domain.usecases.saving_usecases

import fhn.thaobt5.personalfinancetracker.domain.models.SavingGoal
import fhn.thaobt5.personalfinancetracker.domain.repository.UserRepository
import javax.inject.Inject

class AddSavingGoalFireBaseUseCase @Inject constructor(
    private val userRepository: UserRepository
) {
    suspend operator fun invoke(savingGoal: SavingGoal) =
        userRepository.addSavingGoalFireBase(savingGoal)
}