package fhn.thaobt5.personalfinancetracker.domain.utils

enum class TransactionTag(
    val type: String
) {
    HOUSING("Housing"),
    TRANSPORTATION("Transportation"),
    FOOD("Food"),
    UTILITIES("Utilities"),
    INSURANCE("Insurance"),
    HEALTHCARE("Healthcare"),
    SAVING_DEBTS("Saving & Debts"),
    PERSONAL_SPENDING("Personal Spending"),
    ENTERTAINMENT("Entertainment"),
    OTHERS("Others")
}