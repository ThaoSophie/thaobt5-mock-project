package fhn.thaobt5.personalfinancetracker.domain.usecases.user_usecase

import fhn.thaobt5.personalfinancetracker.data.local.SharedPreferenceHelper
import fhn.thaobt5.personalfinancetracker.domain.repository.UserRepository
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import javax.inject.Inject

class LoginUserUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val sharedPreferenceHelper: SharedPreferenceHelper
) {
    suspend operator fun invoke(email: String, password: String, savePassChecked: Boolean): FireBaseState<String> {
        return when {
            email.isEmpty() -> FireBaseState.Fail("Email must not be empty")
            password.isEmpty() -> FireBaseState.Fail("Password must not be empty")
            else -> {
                val loginResponseState = userRepository.login(email, password)
                if (loginResponseState is FireBaseState.Success) {
                    sharedPreferenceHelper.setEmail(email)
                    sharedPreferenceHelper.setPassword(password)
                    sharedPreferenceHelper.setIsLoggedIn(true)
                    sharedPreferenceHelper.setIsRemember(savePassChecked)
                    sharedPreferenceHelper.setIsOpenFirstTime(false)
                }
                return loginResponseState
            }

        }
    }


}