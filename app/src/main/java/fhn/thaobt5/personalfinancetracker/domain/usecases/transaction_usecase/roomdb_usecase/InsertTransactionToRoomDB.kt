package fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.roomdb_usecase

import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.repository.TransactionRepository
import javax.inject.Inject

class InsertTransactionToRoomDB @Inject constructor(
    private val transactionRepo: TransactionRepository
) {

    suspend operator fun invoke(transaction: Transaction) {
        transactionRepo.insert(transaction)

    }
}