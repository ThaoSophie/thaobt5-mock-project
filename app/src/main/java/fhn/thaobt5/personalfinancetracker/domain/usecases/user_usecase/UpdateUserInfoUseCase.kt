package fhn.thaobt5.personalfinancetracker.domain.usecases.user_usecase

import fhn.thaobt5.personalfinancetracker.data.local.SharedPreferenceHelper
import fhn.thaobt5.personalfinancetracker.domain.models.User
import fhn.thaobt5.personalfinancetracker.domain.repository.UserRepository
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import javax.inject.Inject

class UpdateUserInfoUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val sharedPreferenceHelper: SharedPreferenceHelper
) {

    suspend operator fun invoke(
        balance: Long,
        expense: Long,
        income: Long
    ) {
        userRepository.updateUserBalance(balance)
        userRepository.updateUserExpense(expense)
        userRepository.updateUserIncome(income)
    }
}