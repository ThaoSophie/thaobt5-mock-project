package fhn.thaobt5.personalfinancetracker.domain.repository

import fhn.thaobt5.personalfinancetracker.data.models.api_model.CurrencyResponse
import fhn.thaobt5.personalfinancetracker.utils.api_state.ApiResource

interface CurrencyApiRepository {
    suspend fun getRates(base: String): ApiResource<CurrencyResponse>
}