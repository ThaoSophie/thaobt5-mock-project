package fhn.thaobt5.personalfinancetracker.domain.usecases.currency_api_usecase

import fhn.thaobt5.personalfinancetracker.domain.repository.CurrencyApiRepository
import javax.inject.Inject

class GetRatesFromCurrencyApiUseCase @Inject constructor(
    private val currencyApiRepository: CurrencyApiRepository
) {
    suspend operator fun invoke(base: String) = currencyApiRepository.getRates(base)
}