package fhn.thaobt5.personalfinancetracker.domain.usecases.transaction_usecase.firebase_usecase

import android.annotation.SuppressLint
import android.util.Log
import fhn.thaobt5.personalfinancetracker.domain.models.Transaction
import fhn.thaobt5.personalfinancetracker.domain.repository.UserRepository
import fhn.thaobt5.personalfinancetracker.domain.utils.FireBaseState
import javax.inject.Inject

class UpdateTransactionToFireBase @Inject constructor(
    private val userRepository: UserRepository
) {

    @SuppressLint("LongLogTag")
    suspend operator fun invoke(transaction: Transaction): FireBaseState<String> {
        Log.d("UpdateTransactionToFireBase", "invoke: ")
        return userRepository.updateTransaction(transaction)

    }
}