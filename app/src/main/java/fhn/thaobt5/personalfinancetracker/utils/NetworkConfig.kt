package fhn.thaobt5.personalfinancetracker.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.os.Build
import androidx.annotation.RequiresApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

object NetworkConfig {
    val isNetworkConnected = MutableStateFlow(false)

    private lateinit var connectivityManager: ConnectivityManager
    private lateinit var connectivityCallBack: ConnectivityManager.NetworkCallback
    @RequiresApi(Build.VERSION_CODES.N)
    fun checkNetworkAvailable(context: Context) {
        connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityCallBack = object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                super.onAvailable(network)
                CoroutineScope(Dispatchers.Default).launch {
                    isNetworkConnected.emit(true)
                }
            }

            override fun onLost(network: Network) {
                super.onLost(network)
                CoroutineScope(Dispatchers.Default).launch {
                    isNetworkConnected.emit(false)
                }
            }
        }
        connectivityManager.registerDefaultNetworkCallback(connectivityCallBack)
    }

    fun unregisterNetworkManager(context: Context) {
        connectivityManager.unregisterNetworkCallback(connectivityCallBack)
    }
}