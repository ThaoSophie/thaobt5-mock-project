package fhn.thaobt5.personalfinancetracker.utils.viewState

import fhn.thaobt5.personalfinancetracker.domain.models.Transaction

sealed class DetailState {
    object Loading: DetailState()
    object Empty: DetailState()
    data class Success(val transaction: Transaction): DetailState()
    data class Error(val exception: Throwable): DetailState()
}
