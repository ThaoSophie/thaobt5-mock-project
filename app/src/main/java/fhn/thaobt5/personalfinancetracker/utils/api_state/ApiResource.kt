package fhn.thaobt5.personalfinancetracker.utils.api_state

sealed class ApiResource<T>(val data: T?, val message: String?) {
    class Success<T>(data: T?): ApiResource<T>(data, null)
    class Error<T>(message: String): ApiResource<T>(null, message)

}
