package fhn.thaobt5.personalfinancetracker.utils

import fhn.thaobt5.personalfinancetracker.domain.utils.TransactionTag
import fhn.thaobt5.personalfinancetracker.domain.utils.TransactionType


const val MY_SHARED_PREFERENCE = "My Shared Preference"
const val EMAIL = "USER_EMAIL"
const val PASSWORD = "USER_PASSWORD"
const val USER_NAME = "USER_NAME"
const val REMEMBER = "IS_REMEMBER"
const val IS_LOGGED_IN = "IS_LOGGED_IN"
const val IS_OPEN_FIRST_TIME = "IS_OPEN_FIRST_TIME"

const val TRANSACTION_DB = "transaction.db"
const val USER_DB = "user.db"

object Constants {
    val transactionTypes = listOf(TransactionType.INCOME.name, TransactionType.EXPENSE.name)

    val allTransactionTypes =
        listOf(TransactionType.OVERALL.name, TransactionType.INCOME.name, TransactionType.EXPENSE.name)

    val transactionTags = TransactionTag.values()


//    val transactionTags = listOf(
//        "Housing",
//        "Transportation",
//        "Food",
//        "Utilities",
//        "Insurance",
//        "Healthcare",
//        "Saving & Debts",
//        "Personal Spending",
//        "Entertainment",
//        "Others"
//    )
}

