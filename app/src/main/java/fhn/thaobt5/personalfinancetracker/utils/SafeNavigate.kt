package fhn.thaobt5.personalfinancetracker.utils

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions

fun NavController.safeNavigate(
    direction: NavDirections,
    aim: NavOptions? = null,
) {
    currentDestination?.getAction(direction.actionId)?.run {
        navigate(direction, aim)
    }

}