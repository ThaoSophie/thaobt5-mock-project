package fhn.thaobt5.personalfinancetracker.utils

import android.provider.ContactsContract.CommonDataKinds.Email
import android.util.Patterns
import java.util.regex.Pattern

object InputFieldValidator {
    fun isValidEmail(data: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(data).matches()
    }

    fun isValidPassword(data: String): Boolean {
        val regex = "^(?=\\\\S+\\\$).{8,}"
        return !Pattern.compile(regex).matcher(data).matches()
    }

    fun isMatches(data1: String, data2: String): Boolean {
        return data1 == data2
    }

    fun isValidateAccount(email: String, password: String): Boolean {
        return isValidEmail(email) && isValidPassword(password)
    }

    fun isNotNull(data: String): Boolean {
        return data.isNotEmpty()
    }



}