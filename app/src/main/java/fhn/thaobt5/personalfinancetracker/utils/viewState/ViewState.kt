package fhn.thaobt5.personalfinancetracker.utils.viewState

import fhn.thaobt5.personalfinancetracker.domain.models.Transaction


sealed class ViewState {
    object Loading: ViewState()
    object Empty: ViewState()
    data class Success(val transactions: List<Transaction>): ViewState()
    data class Error(val exception: Throwable): ViewState()
}
