package fhn.thaobt5.personalfinancetracker.utils

import kotlinx.coroutines.CoroutineDispatcher

interface DispatcherProvider {
    val main: CoroutineDispatcher
    val io: CoroutineDispatcher
    val defaultL: CoroutineDispatcher
    val unconfined: CoroutineDispatcher
}