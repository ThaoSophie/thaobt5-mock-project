package fhn.thaobt5.personalfinancetracker.utils.api_state

sealed class CurrencyApiEvent {
    class Success(val resultText: String): CurrencyApiEvent()
    class Failure(val errorText: String): CurrencyApiEvent()
    object Loading: CurrencyApiEvent()
    object Empty: CurrencyApiEvent()

}
