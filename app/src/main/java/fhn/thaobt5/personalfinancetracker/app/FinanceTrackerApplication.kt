package fhn.thaobt5.personalfinancetracker.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class FinanceTrackerApplication: Application() {
}